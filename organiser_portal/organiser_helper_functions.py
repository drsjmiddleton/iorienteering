__author__ = 'drsjmiddleton'

import pyqrcode
from iOrienteering.static_values import *
import io, tempfile
from website.models import OrderedCheckpoint
from website.website_helper_functions import *
#from organiser_portal.organiser_json_encoders import
from website.serializers import QRCourseSerializer

from PIL import Image, ImageDraw, ImageFont
from django.contrib.staticfiles.templatetags.staticfiles import static
from reportlab.lib.utils import ImageReader
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm

import json


def createListOfOrderededCheckpointsFromString(order_string, this_course, this_area):

    if len(order_string) % 3 != 0:
        return None

    available_checkpoints = list(this_area.checkpoint_set.all())

    listofOrderedCheckpoints = []

    for i in range(0, len(order_string) // 3):

        tempCheckpointIdStr = order_string[i * 3:(i * 3) + 3]

        for cp in available_checkpoints:
            if cp.checkpoint_ID==tempCheckpointIdStr:
                listofOrderedCheckpoints.append(OrderedCheckpoint(checkpoint = cp ,
                                                                    course = this_course,
                                                                    order_number = i))

    return listofOrderedCheckpoints

def getOrderedCheckpointList(course):

    listOfCheckpoints = list(course.orderedcheckpoint_set.all().order_by('order_number'))

    return listOfCheckpoints


def getCheckpointInListFromID(listOfCheckpoints, ID):

    for cp in listOfCheckpoints:
        if cp.checkpoint_ID==ID:
            return cp

    return None

def filterAndSortCheckpointTimeList(this_competitor):


    cpt_list = list(this_competitor.checkpointtime_set.all().order_by('checkpoint__order_number'))

    return

def validateCourse(this_course, new_checkpoint):

        listOfOrderedCheckpoints = list(this_course.orderedcheckpoint_set.all())

        listOfCheckpoints=[]

        for cp in listOfOrderedCheckpoints:
            if not cp.checkpoint in listOfCheckpoints:
                listOfCheckpoints.append(cp.checkpoint)

        if new_checkpoint in listOfCheckpoints:
            listOfCheckpoints.remove(new_checkpoint)
            listOfCheckpoints.append(new_checkpoint)
        else:
            return True


        #check it has the right number of start/finish/psc
        startCount =0
        finishCount =0


        for checkpoint in listOfCheckpoints:
            if checkpoint.is_start:
                startCount+=1
            if checkpoint.is_finish:
                finishCount+=1


        if startCount!=1:
            return False

        if finishCount!=1:
            return False


        return True


def getQRPNGforCheckpoint(checkpoint):
    cpQR=pyqrcode.create(QR_URL+checkpoint.checkpoint.checkpoint_ID)
    temp_file = tempfile.SpooledTemporaryFile()
    cpQR.png(file=temp_file)
    return temp_file


def getQRPNGforCourseSetup(this_course):

    serializer = QRCourseSerializer(this_course)
    course_info = json.dumps(serializer.data)

    qr_string = QR_URL+QR_COURSE_SETUP+course_info
    cpQR = pyqrcode.create(qr_string)
    temp_file = tempfile.SpooledTemporaryFile()
    cpQR.png(file=temp_file)
    return temp_file

def getCheckpointOrderString(this_course):
    cp_list  = getOrderedCheckpointList(this_course)
    order_string=''
    for cp in cp_list:
        order_string+=cp.checkpoint.checkpoint_ID
    return order_string

import os
from iOrienteering import settings
from pathlib import Path
from django.core.files.storage import get_storage_class

def getQRLabel(label_string, font_size):

    #static_storage = get_storage_class(settings.STATICFILES_STORAGE)()
    #key_path = Path(static_storage.base_location)

    #font_path = key_path / 'website/assets/fonts/arial.ttf'
    #public_path = key_path / 'website/assets/keys/android_test_public.pem'
    #fprivkey = private_path.open()

    arial_ttf_file = os.path.join(settings.STATIC_ROOT, 'website/assets/fonts/arial.ttf')

    #arial_ttf_file = open(static('website/assets/fonts/arial.ttf'))

    arial_font = ImageFont.truetype(arial_ttf_file, size=font_size)

    qr_label = Image.new("RGB", (1000, 1000), "white")

    draw = ImageDraw.Draw(qr_label)

    draw.multiline_text((0, 0), label_string, fill="black", font=arial_font, spacing=3, align="center")

    text_size = draw.multiline_textsize(label_string, font=arial_font, spacing=3)

    qr_label = qr_label.crop((0,0,text_size[0], text_size[1]))

    qr_label = ImageReader(qr_label)

    return qr_label



def get_date_for_js(this_datetime):

    return this_datetime.strftime('%d/%m/%Y %H:%M:%S')

# class ParagraphStyle(PropertySet):
#  defaults = {
#  'fontName':'Times-Roman',
#  'fontSize':10,
#  'leading':12,
#  'leftIndent':0,
#  'rightIndent':0,
#  'firstLineIndent':0,
#  'alignment':TA_LEFT,
#  'spaceBefore':0,
#  'spaceAfter':0,
#  'bulletFontName':'Times-Roman',
#  'bulletFontSize':10,
#  'bulletIndent':0,
#  'textColor': black,
#  'backColor':None,
#  'wordWrap':None,
#  'borderWidth': 0,
#  'borderPadding': 0,
#  'borderColor': None,
#  'borderRadius': None,
#  'allowWidows': 1,
#  'allowOrphans': 0,
#  'textTransform':None,
#  'endDots':None,
#  'splitLongWords':1,
#  'underlineProportion': _baseUnderlineProportion,
#  'bulletAnchor': 'start',
#  }
