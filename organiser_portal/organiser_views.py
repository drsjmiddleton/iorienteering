__author__ = 'drsjmiddleton'

from django.shortcuts import render

from organiser_portal.organiser_helper_functions import *
from organiser_portal.organiser_json_encoders import *


from website.models import *


from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.files.storage import get_storage_class
from pathlib import Path

from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, JsonResponse
from django.core.urlresolvers import reverse
from django.core import serializers
from django.contrib import messages
from django.contrib.messages import get_messages
from django.contrib.auth.decorators import login_required
from django.db.models import Q

from organiser_portal.organiser_forms import *
from django.contrib.auth import authenticate, login, logout

from rest_framework.authtoken.models import Token


from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError

from rest_framework.decorators import api_view, authentication_classes
from rest_framework import status
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication




import csv
import json

from decimal import *
from io import BytesIO

from iOrienteering.static_values import *

from paypal.standard.forms import PayPalPaymentsForm
from paypal.standard.models import ST_PP_COMPLETED
from paypal.standard.ipn.signals import valid_ipn_received, invalid_ipn_received

from website.paypal import credit_user_account

# Create your views here.


@login_required
def organiser_home(request):

    this_user = request.user

    this_user_profile = get_object_or_404(UserProfile, user=this_user)

    this_user_profile.display_credit="{0:.2f}".format(this_user_profile.credit)


    this_user.number_of_areas = this_user.area_set.all().count()

    messages = list(UserMessage.objects.filter((Q(universal_message=True) | Q(user=this_user))))


    warning_messages = []
    info_messages = []

    for message in list(messages):

        if message.expire_date<date.today():

            message.delete()

        elif message.important:
           warning_messages.append(message)
        else:
            info_messages.append(message)

    warning_messages.sort(key=lambda x: x.message_date, reverse=True)
    info_messages.sort(key=lambda x: x.message_date, reverse=True)

    context = {'this_user':this_user, 'info_messages':info_messages,'warning_messages':warning_messages, 'this_user_profile':this_user_profile}

    return render(request, 'organiser/organiser_home.html', context)

@login_required
def areas(request):

    this_user = request.user

    areas = this_user.area_set.all()

    info_message = None

    storage = get_messages(request)
    for message in storage:
        info_message=message

    context = {'this_user':this_user, 'info_message':info_message, 'areas':areas}

    return render(request, 'organiser/areas.html', context)

@login_required
def new_area(request):

    this_user = request.user

    user_areas = list(this_user.area_set.all())

    other_areas = list(Area.objects.all().exclude(user=this_user))

    temp_area = Area(user=this_user)

    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        new_area_form = CreateAreaForm(request.POST, instance=temp_area)

        if new_area_form.is_valid():
            tempArea = new_area_form.save(commit=False)

                # any pre save code

            tempArea.save()

            return HttpResponseRedirect(reverse('main_area_screen', kwargs={'area_pk': tempArea.id}))

    # if a GET (or any other method) we'll create a blank form
    else:
        new_area_form = CreateAreaForm(instance=temp_area)

    json_user_areas = json.dumps(user_areas, cls=AreaEncoder)

    json_other_areas = json.dumps(other_areas, cls=AreaEncoder)

    context = {'this_user':this_user, 'new_area_form':new_area_form, 'json_user_areas':json_user_areas, 'json_other_areas':json_other_areas}

    return render(request, 'organiser/new_area.html', context)



@login_required
def edit_area(request, area_pk):
    pass








# views from area root

@login_required
def main_area_screen(request, area_pk):

    this_user = request.user

    this_user_profile = get_object_or_404(UserProfile, user=this_user)

    this_area = get_object_or_404(Area, pk=area_pk)

    courses = list(this_area.course_set.all())

    for course in courses:
        if course.coursecompetitor_set.all().count()>0:
            course.fixed=True

    checkpoints = list(this_area.checkpoint_set.all())

    start_count = 0

    finish_count = 0

    can_create_course = None

    for cp in checkpoints:
        if cp.is_start:
            start_count+=1
            continue
        if cp.is_finish:
            finish_count+=1

    if (start_count>0) and (finish_count>0):
        can_create_course=True

    maps = list(this_area.map_set.all())

    error_message = None

    storage = get_messages(request)
    for message in storage:
        error_message = message

    context = {'user': this_user, 'this_area': this_area, "checkpoints": checkpoints, 'courses': courses, 'can_create_course':can_create_course, 'error_message':error_message}

    return render(request, 'organiser/main_area_screen.html', context)

@login_required
def public_area_home(request, area_pk):
    pass

@login_required
def checkpoints(request, area_pk):
    user = request.user
    this_area = get_object_or_404(Area, pk=area_pk)
    checkpoints = this_area.checkpoint_set.all()

    error_message = None

    storage = get_messages(request)
    for message in storage:
        error_message=message

    context = {'user':user , 'this_area': this_area, "checkpoints":checkpoints, 'error_message':error_message}

    return render(request, 'website/checkpoints.html', context)
@login_required
def new_checkpoint(request, area_pk):

    user = request.user
    this_area = get_object_or_404(Area, pk=area_pk)

    tempCheckpoint = Checkpoint(area=this_area)

    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        checkpointForm = CreateCheckpointForm(request.POST , instance=tempCheckpoint)
        # check whether it's valid:
        if checkpointForm.is_valid():

            newCp = checkpointForm.save()

            if newCp.checkpoint_name == None or newCp.checkpoint_name == '':
                newCp.checkpoint_name = newCp.checkpoint_ID
                newCp.save()

            return HttpResponseRedirect(reverse('main_area_screen', kwargs={'area_pk': this_area.id}))

    # if a GET (or any other method) we'll create a blank form
    else:
        checkpointForm = CreateCheckpointForm(instance=tempCheckpoint)


    checkpointForm.helper.form_action = reverse('new_checkpoint', kwargs={'area_pk': this_area.id})

    context = {'user':user , 'this_area': this_area , 'checkpointForm':checkpointForm }

    return render(request, 'organiser/new_checkpoint.html', context)
@login_required
def edit_checkpoint(request, area_pk, checkpoint_pk):

    user = request.user
    this_area = get_object_or_404(Area, pk=area_pk)
    this_checkpoint = get_object_or_404(Checkpoint, pk=checkpoint_pk)
    old_ID = this_checkpoint.checkpoint_ID

    course_error = None

    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        checkpointForm = CreateCheckpointForm(request.POST , instance=this_checkpoint)
        # check whether it's valid:
        if checkpointForm.is_valid():

            this_checkpoint = checkpointForm.save(commit=False)

            if this_checkpoint.checkpoint_name == None or this_checkpoint.checkpoint_name == '':
                this_checkpoint.checkpoint_name = this_checkpoint.checkpoint_ID

            course_errors = False

            for course in this_area.course_set.all():

                if not validateCourse(this_course=course, new_checkpoint = this_checkpoint):
                    course_errors = True

                if not course_errors:
                    course.save()

                else:
                    break

            if course_errors:
                course_error = "This action will invalidate courses which this checkpoint is part of"

            else:
                this_checkpoint.save()
                return HttpResponseRedirect(reverse('main_area_screen', kwargs={'area_pk': this_area.id}))

    # if a GET (or any other method) we'll create a blank form
    else:
        checkpointForm = CreateCheckpointForm(instance=this_checkpoint)

    checkpointForm.helper.form_action = reverse('edit_checkpoint', kwargs={'area_pk': this_area.id, 'checkpoint_pk':this_checkpoint.id})

    context = {'user':user , 'this_area': this_area , 'checkpointForm':checkpointForm, 'this_checkpoint':this_checkpoint, 'course_error': course_error}

    return render(request, 'organiser/edit_checkpoint.html', context)
@login_required
def delete_checkpoint(request, area_pk, checkpoint_pk):

    user = request.user
    this_area = get_object_or_404(Area, pk=area_pk)
    this_checkpoint = get_object_or_404(Checkpoint, pk=checkpoint_pk)

    if this_checkpoint.orderedcheckpoint_set.all().count()>0:
        messages.error(request,"This checkpoint is currently part of 1 or more courses - please remove it from these prior to delete")
        return HttpResponseRedirect(reverse('main_area_screen', kwargs={'area_pk': this_area.id}))

    this_checkpoint.delete()

    return HttpResponseRedirect(reverse('main_area_screen', kwargs={'area_pk': this_area.id}))

@login_required
def courses(request, area_pk):
    user = request.user
    this_area = get_object_or_404(Area, pk=area_pk)

    courses = []

    score_courses = []

    for course in this_area.course_set.all():

        if course.is_score:
            score_courses.append(course)

        else:
            courses.append(course)


    for course in courses:
        course.orderedCourse = getOrderedCheckpointList(course=course)

    for score_course in score_courses:
        score_course.checkpoint_list = []
        for checkpoint in list(score_course.checkpoints.all()):
            score_course.checkpoint_list.append(checkpoint)

    context = {'user':user , 'this_area': this_area, "courses":courses, 'score_courses':score_courses}

    return render(request, 'website/courses.html', context)
@login_required
def new_course(request, area_pk):

    user = request.user
    this_area = get_object_or_404(Area, pk=area_pk)

    checkpointList = this_area.checkpoint_set.all()

    tempCourse = Course(area=this_area)

    dictionary_of_course_name_ID = {}

    dictionary_of_course_ID_type = {}

    dictionary_of_course_ID_name = {}

    checkpoint_order_string=None

    for checkpoint in checkpointList:
        dictionary_of_course_name_ID[checkpoint.checkpoint_name] = checkpoint.checkpoint_ID
        dictionary_of_course_ID_name[checkpoint.checkpoint_ID] = checkpoint.checkpoint_name

        if checkpoint.is_start:
            checkpoint_type=1
        elif checkpoint.is_finish:
            checkpoint_type=2
        else:
            checkpoint_type=0

        dictionary_of_course_ID_type[checkpoint.checkpoint_ID] = checkpoint_type


    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        courseForm = CreateCourseForm(request.POST , instance=tempCourse)

        checkpoint_order_string = courseForm.data.get("checkpoint_order")

        # check whether it's valid:
        if courseForm.is_valid():

            tempCourse=courseForm.save()

            for ocp in createListOfOrderededCheckpointsFromString(courseForm.cleaned_data.get("checkpoint_order"), this_course=tempCourse, this_area=this_area):
                ocp.save()

            return HttpResponseRedirect(reverse('main_area_screen', kwargs={'area_pk': this_area.id}))

    # if a GET (or any other method) we'll create a blank form
    else:
        courseForm = CreateCourseForm(instance=tempCourse)

        courseForm.helper.form_action = reverse('new_course',kwargs={'area_pk': this_area.id})

    context = {'user':user , 'this_area': this_area , 'courseForm':courseForm,
               'dictionary_of_course_ID_type':dictionary_of_course_ID_type, 'checkpoint_list':checkpointList,
               'checkpoint_order_string':checkpoint_order_string, 'dictionary_of_course_ID_name':dictionary_of_course_ID_name}

    return render(request, 'organiser/new_course.html', context)
@login_required
def edit_course(request, area_pk, course_pk):

    user = request.user
    this_area = get_object_or_404(Area, pk=area_pk)
    this_course = get_object_or_404(Course, pk=course_pk)
    checkpointList = this_area.checkpoint_set.all()


    dictionary_of_course_name_ID = {}

    dictionary_of_course_ID_type = {}

    dictionary_of_course_ID_name = {}

    checkpoint_order_string = ''

    for ocp in getOrderedCheckpointList(this_course):
        checkpoint_order_string+=ocp.checkpoint.checkpoint_ID

    for checkpoint in checkpointList:
        dictionary_of_course_name_ID[checkpoint.checkpoint_name] = checkpoint.checkpoint_ID
        dictionary_of_course_ID_name[checkpoint.checkpoint_ID] = checkpoint.checkpoint_name

        if checkpoint.is_start:
            checkpoint_type = 1
        elif checkpoint.is_finish:
            checkpoint_type = 2
        else:
            checkpoint_type = 0

        dictionary_of_course_ID_type[checkpoint.checkpoint_ID] = checkpoint_type

    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        courseForm = CreateCourseForm(request.POST, instance=this_course)

        checkpoint_order_string = courseForm.data.get("checkpoint_order")

        # check whether it's valid:
        if courseForm.is_valid():

            this_course.delete()

            this_course = courseForm.save()

            for ocp in createListOfOrderededCheckpointsFromString(courseForm.cleaned_data.get("checkpoint_order"),
                                                                  this_course=this_course, this_area=this_area):
                ocp.save()

            return HttpResponseRedirect(reverse('main_area_screen', kwargs={'area_pk': this_area.id}))

    # if a GET (or any other method) we'll create a blank form
    else:
        courseForm = CreateCourseForm(instance=this_course)


    courseForm.helper.form_action = reverse('edit_course', kwargs={'area_pk': this_area.id, 'course_pk':this_course.id})



    context = {'user': user, 'this_area': this_area, 'courseForm': courseForm,
               'dictionary_of_course_ID_type': dictionary_of_course_ID_type, 'checkpoint_list': checkpointList,
               'checkpoint_order_string': checkpoint_order_string,
               'dictionary_of_course_ID_name': dictionary_of_course_ID_name}

    return render(request, 'organiser/new_course.html', context)


@login_required
def delete_course(request, area_pk, course_pk):

    user = request.user
    this_area = get_object_or_404(Area, pk=area_pk)
    this_course = get_object_or_404(Course, pk=course_pk)

    this_course.delete()

    return HttpResponseRedirect(reverse('main_area_screen', kwargs={'area_pk': this_area.id}))


@login_required
def organiser_area_results(request, area_pk):

    this_area = get_object_or_404(Area, pk=area_pk)

    user = request.user

    # Get selects for course
    standardCourseList = []
    scoreCourseList = []

    for course in this_area.course_set.all().exclude(is_score=True):
        standardCourseList.append((course.course_name, course.course_name))

    for course in this_area.course_set.all().exclude(is_score=False):
        scoreCourseList.append((course.course_name, course.course_name))

    standard_filter_form = OrganiserStandardResultsFilterForm(courseList=standardCourseList, initial={'age_filter': ['All']})

    score_filter_form = OrganiserScoreResultsFilterForm(courseList=scoreCourseList,
                                                              initial={'age_filter': ['All']})



    context = {'user': user, 'standard_filter_form':standard_filter_form, 'score_filter_form':score_filter_form, 'this_area': this_area}

    return render(request, 'organiser/area_results.html', context)


@login_required
def ajax_organiser_area_results(request, area_pk):

    this_area = get_object_or_404(Area, pk=area_pk)

    complete_area_results = []
    not_finished_results = []

    for course in this_area.course_set.all().filter(is_score=False):
        for result in course.result_set.all():
            complete_area_results.append(result)


    for result in list(complete_area_results):
        if result.status>0:
            not_finished_results.append(result)
            complete_area_results.remove(result)

    complete_area_results.sort(key=lambda x: x.run_time, reverse=False)

    for result in not_finished_results:
        complete_area_results.append(result)

    json_area_results = json.dumps(complete_area_results, cls=AreaResultsEncoder)

    return HttpResponse(json_area_results, content_type="application/json")

@login_required
def ajax_organiser_area_score_results(request, area_pk):

    this_area = get_object_or_404(Area, pk=area_pk)

    complete_area_results = []
    not_finished_results = []

    for course in this_area.course_set.all().filter(is_score=True):
        for result in course.result_set.all():
            complete_area_results.append(result)


    for result in list(complete_area_results):
        if result.status>0:
            not_finished_results.append(result)
            complete_area_results.remove(result)

    complete_area_results.sort(key=lambda x: x.points, reverse=True)

    for result in not_finished_results:
        complete_area_results.append(result)

    json_area_results = json.dumps(complete_area_results, cls=AreaResultsEncoder)

    return HttpResponse(json_area_results, content_type="application/json")



from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm
from reportlab.lib.utils import ImageReader, getBytesIO


@login_required
def course_qr_codes(request, area_pk, course_pk):

    user = request.user
    this_area = get_object_or_404(Area, pk=area_pk)
    this_course = get_object_or_404(Course, pk=course_pk)
    course_checkpoints = getOrderedCheckpointList(course=this_course)
    filename = this_area.area_name + '-' + this_course.course_name+ '.pdf"'
    response = HttpResponse(content_type='application/pdf')

    response['Content-Disposition'] = 'attachment; filename="'+filename

    buffer = BytesIO()

    myCanvas = canvas.Canvas(buffer, pagesize=A4)

    #get setup image

    setup_image = ImageReader(getQRPNGforCourseSetup(this_course=this_course))


    if(len(this_course.course_name)>16):
        appropriate_font_size = 50-(len(this_course.course_name))
    else:
        appropriate_font_size = 50

    setup_image_label = getQRLabel(this_course.course_name + "\nSETUP SCAN", int(appropriate_font_size))

    myCanvas.drawImage(setup_image, (A4[0]-(15*cm))/2, (A4[1]-(13*cm))/2, width=15*cm, height=15*cm)

    label_size = myCanvas.drawImage(setup_image_label, ((A4[0] - setup_image_label._image.width)) / 2, (A4[1] - (19 * cm)) / 2)

    myCanvas.showPage()

    #get checkpoint images

    for cp in course_checkpoints:

        cp_image = ImageReader(getQRPNGforCheckpoint(checkpoint=cp))

        cp_label = getQRLabel(cp.checkpoint.checkpoint_ID, 100)

        myCanvas.drawImage(cp_image, (A4[0] - (15 * cm)) / 2, (A4[1] - (13 * cm)) / 2, width=15 * cm, height=15 * cm)

        myCanvas.drawImage(cp_label, (A4[0] - cp_label._image.width)/2, (A4[1] - (19 * cm)) / 2)

        myCanvas.showPage()

    # Close the PDF object cleanly, and we're done.

    myCanvas.save()
    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)
    return response

@login_required
def new_map(request, area_pk):
    user = request.user
    this_area = get_object_or_404(Area, pk=area_pk)

    tempMap = Map(user=user, area = this_area)

    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        mapForm = CreateMapForm(request.POST, instance=tempMap)
        # check whether it's valid:
        if mapForm.is_valid():
            newCp = mapForm.save()

            return HttpResponseRedirect(reverse('main_area_screen', kwargs={'area_pk': this_area.id}))

    # if a GET (or any other method) we'll create a blank form
    else:
        mapForm = CreateMapForm(instance=tempMap)

        mapForm.helper.form_action = reverse('new_checkpoint', kwargs={'area_pk': this_area.id})

    context = {'user': user, 'this_area': this_area, 'mapForm': mapForm}

    return render(request, 'organiser/new_map.html', context)


    return HttpResponseRedirect(reverse('main_area_screen', kwargs={'area_pk': this_area.id}))


@login_required
def edit_map(request, area_pk, map_pk):

    user = request.user
    this_area = get_object_or_404(Area, pk=area_pk)
    this_map = get_object_or_404(Map, pk=map_pk)



    return HttpResponseRedirect(reverse('main_area_screen', kwargs={'area_pk': this_area.id}))


@login_required
def delete_map(request, area_pk, map_pk):

    user = request.user
    this_area = get_object_or_404(Area, pk=area_pk)
    this_map = get_object_or_404(Map, pk=map_pk)

    this_map.delete()

    return HttpResponseRedirect(reverse('main_area_screen', kwargs={'area_pk': this_area.id}))

@login_required
def area_maps(request, area_pk):

    user = request.user
    this_area = get_object_or_404(Area, pk=area_pk)

    return HttpResponseRedirect(reverse('main_area_screen', kwargs={'area_pk': this_area.id}))







