__author__ = 'drsjmiddleton'

__author__ = 'drsjmiddleton'



from django.forms import ModelForm
from django.core.validators import MinValueValidator, MaxValueValidator, RegexValidator

from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.models import User

from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import *
from crispy_forms.bootstrap import PrependedText

from datetime import datetime
import re

from iOrienteering.static_values import *
from website.models import *
from organiser_portal.organiser_helper_functions import *



class CreateAreaForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(CreateAreaForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_class = 'form-horizontal group-border hover-stripped'
        self.helper.label_class = 'col-lg-4 col-md-4 col-sm-12 control-label'
        self.helper.field_class = 'col-lg-8 col-md-8'
        self.helper.add_input(Submit('submit', 'Submit'))

    def clean(self):
        cleaned_data = super(CreateAreaForm, self).clean()
        area_name = cleaned_data.get("area_name")
        user = self.instance.user

        for area in user.area_set.all():

            if area.area_name == area_name:

                if area.id != self.instance.id:
                    raise forms.ValidationError("Area with that name already exists")

    class Meta:
        model = Area
        exclude = ['user']

class CreateCheckpointForm(ModelForm):

    checkpoint_name = forms.CharField(required=False, label="Control Name", help_text="Leave blank to make this the control ID")

    def __init__(self, *args, **kwargs):
        super(CreateCheckpointForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_class = 'form-horizontal group-border hover-stripped'
        self.helper.label_class = 'col-lg-2 col-md-2 col-sm-12 control-label'
        self.helper.field_class = 'col-lg-4 col-md-4'
        self.helper.add_input(Submit('submit', 'Submit'))

    def clean(self):
        cleaned_data = super(CreateCheckpointForm, self).clean()
        checkpoint_name = cleaned_data.get("checkpoint_name")
        is_start = cleaned_data.get("is_start")
        is_finish = cleaned_data.get("is_finish")
        checkpoint_ID = cleaned_data.get("checkpoint_ID")
        area = self.instance.area
        thisCheckpointId = self.instance.id

        if checkpoint_name==None or checkpoint_name=='':
            checkpoint_name=checkpoint_ID


        if is_start:
            if is_finish:
                raise forms.ValidationError("Checkpoint can only be 1 special function")
        if is_finish:
            if is_start:
                raise forms.ValidationError("Checkpoint can only be 1 special function")


        for checkpoint in area.checkpoint_set.all():

            if checkpoint.checkpoint_ID == checkpoint_ID:

                if checkpoint.id!=thisCheckpointId:

                    raise forms.ValidationError("Checkpoint with that ID already exists")

            if checkpoint.checkpoint_name == checkpoint_name:

                if checkpoint.id != thisCheckpointId:
                    raise forms.ValidationError("Checkpoint with that name already exists")
    class Meta:
        model = Checkpoint
        exclude=['area', 'course']

class CreateCourseForm(ModelForm):

    is_score = forms.BooleanField(label="Score Course?", required=False)
    time_limit = forms.IntegerField(initial=60, label="Time Limit (mins)")
    penalty = forms.FloatField(initial=0.2, label="Penalty (points/sec)")
    checkpoint_order=forms.CharField(widget=forms.HiddenInput(), required=False)

    def __init__(self, *args, **kwargs):

        super(CreateCourseForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_method = 'post'
        self.helper.form_id = 'course_form'
        self.helper.form_class = 'form-horizontal group-border hover-stripped'
        self.helper.label_class = 'col-lg-4 col-md-4 col-sm-12 control-label'
        self.helper.field_class = 'col-lg-8 col-md-8'
        self.helper.add_input(Submit('submit', 'Submit'))


    def clean(self):
        cleaned_data = super(CreateCourseForm, self).clean()
        checkpoint_order_string = cleaned_data.get("checkpoint_order")
        courseName = cleaned_data.get("course_name")
        area = self.instance.area
        thisCourseId = self.instance.id

        #check it has a unique name and id for the race

        if courseName and area:

            for course in area.course_set.all():

                if course.course_name.lower() == courseName.lower():

                    if course.id!=thisCourseId:

                        raise forms.ValidationError("Course with that name already exists")

        if not checkpoint_order_string or checkpoint_order_string=='':

                raise forms.ValidationError("A course cannot have no checkpoints.  Please select some from the list.")

        #check it has the right number of start/finish/psc
        startCount =0
        finishCount =0


        listOfCheckpoints = createListOfOrderededCheckpointsFromString(order_string=checkpoint_order_string, this_course=self.instance, this_area=area)

        list_of_checkpoint_codes=[]

        for ocp in listOfCheckpoints:

            if ocp.checkpoint.is_start:
                startCount+=1
            if ocp.checkpoint.is_finish:
                finishCount+=1
            list_of_checkpoint_codes.append(ocp.checkpoint.checkpoint_ID)

        if startCount!=1:
            raise forms.ValidationError("A course must have 1 start")

        if finishCount!=1:
            raise forms.ValidationError("A course must have 1 finish")


        #check the order makes sense

        if not listOfCheckpoints[0].checkpoint.is_start:
            raise forms.ValidationError("Start is in the wrong place")

        if not listOfCheckpoints[-1].checkpoint.is_finish:
            raise forms.ValidationError("Finish is in the wrong place")

        if cleaned_data.get("is_score"):
            if(len(list_of_checkpoint_codes) != len(set(list_of_checkpoint_codes))):
                raise forms.ValidationError("A score course can contain only 1 of each checkpoint")


    class Meta:
        model = Course
        exclude=['area']


class CreateMapForm(ModelForm):

    def __init__(self, *args, **kwargs):

        super(CreateMapForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_method = 'post'
        self.helper.form_class = 'form-horizontal group-border hover-stripped'
        self.helper.label_class = 'col-lg-2 col-md-2 col-sm-12 control-label'
        self.helper.field_class = 'col-lg-6 col-md-6'

    def clean(self):
        cleaned_data = super(CreateMapForm, self).clean()
        mapName = cleaned_data.get("map_name")
        area = self.instance.area
        thisMapId = self.instance.id

        #check it has a unique name and id for the race

        if mapName and area:

            for map in area.map_set.all():

                if map.map_name.lower() == mapName.lower():

                    if map.id!=thisMapId:

                        raise forms.ValidationError("Map with that name already exists")



    class Meta:
        model = Map
        exclude=['area', 'user']


class OrganiserStandardResultsFilterForm(forms.Form):

    standard_gender_filter = forms.ChoiceField(required=False, widget=forms.SelectMultiple, label="Gender")
    standard_age_filter = forms.ChoiceField(required=False, widget=forms.SelectMultiple, label="Age")
    standard_course_filter = forms.ChoiceField(required=False, widget=forms.Select, label="Course")
    #include_ns = forms.BooleanField(required=False,label="Include DNS")


    def __init__(self, *args, **kwargs):

        courseList = kwargs.pop('courseList')
        super(OrganiserStandardResultsFilterForm, self).__init__(*args, **kwargs)
        self.fields['standard_age_filter'].choices = AGE_HEADERS
        self.fields['standard_course_filter'].choices = courseList
        self.fields['standard_gender_filter'].choices = (('M', 'Male'),('F', 'Female'),)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_class = 'form-horizontal col-lg-12'
        self.helper.label_class = 'control-label'
        self.helper.field_class = 'controls'

        self.helper.layout = Layout(

                Div('standard_age_filter',css_class='col-lg-2 col-md-2',),
                Div('standard_course_filter',css_class='col-lg-2 col-md-2',),
                Div('standard_gender_filter',css_class='col-lg-2 col-md-2',),
                #Div('include_ns',css_class='col-lg-2 col-md-2', style='margin-top: 16px;'),

        )

class OrganiserScoreResultsFilterForm(forms.Form):

    score_gender_filter = forms.ChoiceField(required=False, widget=forms.SelectMultiple, label="Gender")
    score_age_filter = forms.ChoiceField(required=False, widget=forms.SelectMultiple, label="Age")
    score_course_filter = forms.ChoiceField(required=False, widget=forms.Select, label="Course")
    #include_ns = forms.BooleanField(required=False,label="Include DNS")


    def __init__(self, *args, **kwargs):

        courseList = kwargs.pop('courseList')
        super(OrganiserScoreResultsFilterForm, self).__init__(*args, **kwargs)
        self.fields['score_age_filter'].choices = AGE_HEADERS
        self.fields['score_course_filter'].choices = courseList
        self.fields['score_gender_filter'].choices = (('M', 'Male'),('F', 'Female'),)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_class = 'form-horizontal col-lg-12'
        self.helper.label_class = 'control-label'
        self.helper.field_class = 'controls'

        self.helper.layout = Layout(

                Div('score_age_filter',css_class='col-lg-2 col-md-2',),
                Div('score_course_filter',css_class='col-lg-2 col-md-2',),
                Div('score_gender_filter',css_class='col-lg-2 col-md-2',),
                #Div('include_ns',css_class='col-lg-2 col-md-2', style='margin-top: 16px;'),

        )