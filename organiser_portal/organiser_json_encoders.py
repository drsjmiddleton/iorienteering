from website.models import *

from organiser_portal.organiser_helper_functions import getOrderedCheckpointList, get_date_for_js

import json

class AreaEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Area):

            in_database = False

            try:
                in_database=(obj.user_competitor!=None)
            except AttributeError:
                pass

            return [str(obj.id),obj.area_name, str(obj.latitude), str(obj.longitude)]
        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)



class AreaResultsEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Result):

            #generate AGE/CLASS string

            effective_age_years = obj.run_date.year - obj.competitor.date_of_birth.year

            if effective_age_years < 11:
                effective_age_years = str(10)
            elif effective_age_years < 21:
                effective_age_years = str(((effective_age_years + 1) // 2) * 2)
            elif effective_age_years > 79:
                effective_age_years = str(80)
            elif effective_age_years > 35:
                effective_age_years = str((effective_age_years // 5) * 5)
            else:
                effective_age_years = str(21)

            age_gender_string = obj.competitor.gender+effective_age_years

            if obj.status>STATUS_COMPLETED:
                display_time=obj.dsq_reason
            else:
                display_time=str(obj.run_time)

            return [obj.competitor.firstname,
                    obj.competitor.surname,
                    obj.competitor.gender,
                    age_gender_string,
                    obj.course.course_name,
                    obj.competitor.club,
                    display_time,
                    obj.splits_xml,
                    str(obj.points),
                    str(obj.status),
                    get_date_for_js(obj.run_date),
                    str(obj.id)]
        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)

class CourseEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Course):

            obj.course_codes=''

            obj.course_points=''

            for cp in getOrderedCheckpointList(course = obj):
                obj.course_codes+=cp.checkpoint_ID
                if(cp.is_start or cp.is_finish):
                    obj.course_points+='0,'
                else:
                    obj.course_points += (cp.points_value+',')

            return [str(obj.pk), str(obj.area.pk), obj.course_name, obj.course_codes, obj.course_points, obj.course_description, str(obj.distance), str(obj.penalty), str(obj.allow_multiple_runs),str(obj.is_score)]
        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)