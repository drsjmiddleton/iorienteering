__author__ = 'drsjmiddleton'

from decimal import *
from datetime import timedelta

USER_TYPE_ORGANISER = 1
USER_TYPE_COMPETITOR = 2

USER_TYPE_CHOICES = (
    (str(USER_TYPE_ORGANISER), 'Organiser'),
    (str(USER_TYPE_COMPETITOR), 'Competitor'),

)

USERMESSAGE_EXPIRY_TIME = timedelta(days=7)

FILTER_GENDER_CHOICES = (
    ('0', 'All'),
    ('1', 'Male'),
    ('2', 'Female'),

)

FORM_GENDER_CHOICES = (

    ('M', 'Male'),
    ('F', 'Female'),

)

AGE_HEADERS=(('10','10'),('12','12'),('14','14'),('16','16'),('18','18'),('20','20'),('21','21'),('35','35'),('40','40'),('45','45'),('50','50'),('55','55'),('60','60'),('65','65'),('70','70'),('75','75'),('80','80'))

MAP_IMAGE_TYPES = ['image/png', 'image/jpeg']
MAP_IMAGE_MAX_SIZE= 2621440

QR_URL = 'www.iOrienteering.com/?'

QR_COURSE_SETUP ='&&&'

QR_SPLITTER='%TU%$HU&£$'

STATUS_COMPLETED = 0;
STATUS_DNF = 1;
STATUS_DSQ = 2;
STATUS_RTD = 3;
STATUS_MISSING = 4;