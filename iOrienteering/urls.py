from django.conf.urls import include, url
from django.contrib import admin

from website import website_views

urlpatterns = [
    # Examples:
    # url(r'^$', 'iOrienteering.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', website_views.landing, name='landing'),


    url(r'^admin/', include(admin.site.urls)),
    url(r'^website/', include('website.urls')),

]
