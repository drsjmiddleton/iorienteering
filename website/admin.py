from django.contrib import admin

from website.models import *

# Register your models here.

admin.site.register(UserProfile)
admin.site.register(Area)
admin.site.register(Checkpoint)
admin.site.register(Result)