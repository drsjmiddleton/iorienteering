__author__ = 'drsjmiddleton'

__author__ = 'drsjmiddleton'

from django.shortcuts import get_object_or_404

from website.models import UserProfile, UserMessage

from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist

from decimal import *

from datetime import date, timedelta


from paypal.standard.models import ST_PP_COMPLETED, ST_PP_PENDING
from paypal.standard.ipn.signals import valid_ipn_received, invalid_ipn_received

def credit_user_account(sender, **kwargs):
    ipn_obj = sender

    user_pk = int(ipn_obj.invoice.split('_')[0])

        #credit the user's account

    credit_amount = Decimal(ipn_obj.mc_gross)

    this_user  = get_object_or_404(User, pk=user_pk)

    messages = this_user.usermessage_set.all()

    message_expire_time = timedelta(days=2)


    if ipn_obj.payment_status == ST_PP_COMPLETED:

        try:
            this_user_profile = UserProfile.objects.get(user=this_user)

        except ObjectDoesNotExist:
            return

        this_user_profile.credit= this_user_profile.credit+credit_amount
        this_user_profile.save()


        confirmed_message = "Your recent payment of £"+str(credit_amount)+" has been confirmed and your account has been credited."

        for message in messages:
            if message.paypal_invoice_id==ipn_obj.invoice:
                message.delete()

        payment_confirmed = UserMessage(message_title="Payment Confirmed",paypal_invoice_id= ipn_obj.invoice, message_text=confirmed_message, message_date=date.today(), expire_date=date.today()+message_expire_time, important=True, user=this_user )
        payment_confirmed.save()


    elif ipn_obj.payment_status == ST_PP_PENDING:

        confirmed_message = "Your recent payment of £"+str(credit_amount)+" is pending, once it has been accepted your account will be credited"

        for message in messages:
            if message.paypal_invoice_id==ipn_obj.invoice:
                message.delete()

        payment_confirmed = UserMessage(message_title="Payment Pending",paypal_invoice_id= ipn_obj.invoice, message_text=confirmed_message, message_date=date.today(), expire_date=date.today()+message_expire_time, important=True, user=this_user )
        payment_confirmed.save()


    else:

        confirmed_message = "Your recent payment of £"+str(credit_amount)+" was rejected by Paypal.  No charge has been made to your account/card."

        for message in messages:
            if message.paypal_invoice_id==ipn_obj.invoice:
                message.delete()

        payment_confirmed = UserMessage(message_title="Payment Rejected",paypal_invoice_id= ipn_obj.invoice, message_text=confirmed_message, message_date=date.today(), expire_date=date.today()+message_expire_time, important=True, user=this_user )
        payment_confirmed.save()

