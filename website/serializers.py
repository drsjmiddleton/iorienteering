from rest_framework import serializers
from website.models import *

class QRCourseSerializer(serializers.ModelSerializer):

    course_codes = serializers.CharField(source='get_course_codes')

    course_points = serializers.CharField(source='get_course_points')

    area_name = serializers.CharField(source='get_area_name')


    class Meta:
        model = Course
        fields = ('pk', 'area','area_name','course_name','distance','allow_multiple_runs','is_score','penalty','time_limit', 'course_codes', 'course_points')
        read_only_fields = ('course_codes','course_points',)

class ViewCourseSerializer(serializers.ModelSerializer):

    course_codes = serializers.CharField(source='get_course_codes')
    course_points = serializers.CharField(source='get_course_points')
    area_name = serializers.CharField(source='get_area_name')

    class Meta:
        model = Course
        fields = ('pk', 'course_description', 'area', 'area_name', 'course_name', 'distance', 'allow_multiple_runs', 'is_score', 'penalty',
                  'time_limit', 'course_codes', 'course_points')
        read_only_fields = ('course_codes', 'course_points',)

class GmapAreaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Area
        fields = ('pk','area_name','latitude','longitude',)
