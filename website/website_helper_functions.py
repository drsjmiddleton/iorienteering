__author__ = 'drsjmiddleton'

from datetime import date, datetime, timedelta, time
from django.http import HttpResponse
from rest_framework.renderers import JSONRenderer

import base64


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """

    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

def unix_time(dt):
    epoch = datetime.utcfromtimestamp(0)
    delta = dt - epoch
    milli = int(delta.total_seconds()*1000)

    return milli

def cp_code_encode(key, string):
    encoded_chars = []
    for i in range(len(string)):
        key_c = key[i % len(key)]
        encoded_c = chr(ord(string[i]) + ord(key_c) % 256)
        encoded_chars.append(encoded_c)
    encoded_string = "".join(encoded_chars)
    return base64.urlsafe_b64encode(encoded_string)


def cp_code_decode(key, string):
    decoded_chars = []
    string = base64.urlsafe_b64decode(string)
    for i in range(len(string)):
        key_c = key[i % len(key)]
        encoded_c = chr(abs(ord(string[i]) - ord(key_c) % 256))
        decoded_chars.append(encoded_c)
    decoded_string = "".join(decoded_chars)
    return decoded_string

def timedelta_to_string_time(td):

    if isinstance(td, timedelta):

        hours = (td.seconds//3600)

        minutes = (td.seconds-(hours*3600))//60

        seconds = td.seconds-(hours*3600)-(minutes*60)

        hours = (td.seconds//3600)+(td.days*24)

        if minutes<10:
            minutes = "0"+str(minutes)

        if seconds <10:
            seconds = "0"+str(seconds)

        return str(hours)+":"+str(minutes)+":"+str(seconds)
    else:
        return str(td)

