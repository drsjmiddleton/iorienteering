__author__ = 'drsjmiddleton'

from django.conf.urls import include, url

from organiser_portal import organiser_views
from competitor_portal import competitor_views
from website import website_views


urlpatterns = [
    # Examples:
    # url(r'^$', 'iOrienteering.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),


    url(r'^login/$', website_views.login_user, name='login_user'),
    url(r'^logout/$', website_views.logout_user, name='logout_user'),
    url(r'^register_organiser/$', website_views.register_user_organiser, name='register_user_organiser'),
    url(r'^register_competitor/$', website_views.register_user_competitor, name='register_user_competitor'),
    url(r'^t_and_c/$', website_views.t_and_c, name='t_and_c'),

    url(r'^profile/$', website_views.edit_profile, name='edit_profile'),
    url(r'^change_password/$', website_views.change_password, name='change_password'),


    url(r'^credit/$', website_views.credit, name='credit'),
    url(r'^credit/notify/', include('paypal.standard.ipn.urls')),
    url(r'^credit/confirm/', website_views.credit_confirm, name='credit_confirm'),
    url(r'^credit/payment_made/', website_views.payment_made, name='payment_made'),
    url(r'^credit/payment_cancelled/', website_views.payment_cancelled, name='payment_cancelled'),




    #URLs originating from an organiser

    url(r'^organiser/home/$', organiser_views.organiser_home, name='organiser_home'),

    url(r'^organiser/areas/$', organiser_views.areas, name='areas'),
    url(r'^organiser/areas/new/$', organiser_views.new_area, name='new_area'),
    url(r'^organiser/areas/(?P<area_pk>\d+)/edit_area/$', organiser_views.edit_area, name='edit_area'),

url(r'^organiser/areas/(?P<area_pk>\d+)/public_area_home/$', organiser_views.public_area_home, name='public_area_home'),



    url(r'^organiser/areas/(?P<area_pk>\d+)/main_area_screen/$', organiser_views.main_area_screen, name='main_area_screen'),




    url(r'^organiser/areas/(?P<area_pk>\d+)/checkpoints/$', organiser_views.checkpoints, name='checkpoints'),
    url(r'^organiser/areas/(?P<area_pk>\d+)/checkpoints/new/$', organiser_views.new_checkpoint, name='new_checkpoint'),
    url(r'^organiser/areas/(?P<area_pk>\d+)/checkpoints/(?P<checkpoint_pk>\d+)/edit/$', organiser_views.edit_checkpoint, name='edit_checkpoint'),
    url(r'^organiser/areas/(?P<area_pk>\d+)/checkpoints/(?P<checkpoint_pk>\d+)/delete/$', organiser_views.delete_checkpoint, name='delete_checkpoint'),
    url(r'^organiser/areas/(?P<area_pk>\d+)/courses/$', organiser_views.courses, name='courses'),
    url(r'^organiser/areas/(?P<area_pk>\d+)/courses/new/$', organiser_views.new_course, name='new_course'),
    url(r'^organiser/areas/(?P<area_pk>\d+)/courses/(?P<course_pk>\d+)/edit/$', organiser_views.edit_course, name='edit_course'),
    url(r'^organiser/areas/(?P<area_pk>\d+)/courses/(?P<course_pk>\d+)/delete/$', organiser_views.delete_course, name='delete_course'),
    url(r'^organiser/areas/(?P<area_pk>\d+)/courses/(?P<course_pk>\d+)/course_qr_codes/$', organiser_views.course_qr_codes, name='course_qr_codes'),
    url(r'^organiser/areas/(?P<area_pk>\d+)/organiser_area_results/$', organiser_views.organiser_area_results, name='organiser_area_results'),
    url(r'^organiser/areas/(?P<area_pk>\d+)/ajax_organiser_area_results/$', organiser_views.ajax_organiser_area_results, name='ajax_organiser_area_results'),
    url(r'^organiser/areas/(?P<area_pk>\d+)/ajax_organiser_area_score_results/$', organiser_views.ajax_organiser_area_score_results, name='ajax_organiser_area_score_results'),



    url(r'^organiser/areas/(?P<area_pk>\d+)/maps/new/$', organiser_views.new_map, name='new_map'),
    url(r'^organiser/areas/(?P<area_pk>\d+)/maps/(?P<map_pk>\d+)/edit/$', organiser_views.edit_map,
        name='edit_map'),
    url(r'^organiser/areas/(?P<area_pk>\d+)/maps/(?P<map_pk>\d+)/delete/$', organiser_views.delete_map,
        name='delete_map'),

    url(r'^organiser/areas/(?P<area_pk>\d+)/maps/$', organiser_views.area_maps,
        name='area_maps'),




#URLs originating from an competitor

    url(r'^competitor/home/$', competitor_views.competitor_home, name='competitor_home'),

url(r'^competitor/maps/$', competitor_views.competitor_maps, name='competitor_maps'),


    #URLs for API

#Competitor API

url(r'^api/competitor_get_token/$', website_views.api_competitor_get_token, name='api_competitor_get_token'),

url(r'^api/competitor_get_area_list/$', competitor_views.api_competitor_get_area_list, name='competitor_get_area_list'),

    url(r'^api/(?P<area_pk>\d+)/get_courses/$', competitor_views.api_competitor_get_area_course_list, name='api_competitor_get_area_course_list'),

url(r'^api/competitor_load_result/$', competitor_views.api_competitor_load_result, name='api_competitor_load_result'),


]
