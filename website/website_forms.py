__author__ = 'drsjmiddleton'

from django.forms import ModelForm
from django.core.validators import MinValueValidator, MaxValueValidator, RegexValidator

from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.models import User
from django.db.models import Q
from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import *
from crispy_forms.bootstrap import PrependedText

from datetime import datetime
import re

from website.models import *

from iOrienteering.static_values import *

class NullCharField(forms.CharField):
    def clean(self, value):
        value = super(NullCharField, self).clean(value)
        if value in forms.fields.EMPTY_VALUES:
            return None
        return value

class LoginForm(forms.Form):

    username = forms.CharField(required=True)
    password = forms.CharField(required=False,max_length=32, widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.label_class = 'col-lg-3 col-md-3 col-sm-12 control-label'
        self.helper.field_class = 'col-lg-9 col-md-9'
        self.helper.add_input(Submit('submit', 'Submit'))


class RegisterOrganiserForm(forms.Form):

    username = forms.CharField(required=True,max_length=30)

    email = forms.EmailField(required=True,max_length=50 )
    firstname = forms.CharField(required=True,max_length=100)
    surname = forms.CharField(required=True,max_length=100)
    password = forms.CharField(required=True,max_length=30, widget=forms.PasswordInput)
    rpt_password = forms.CharField(required=True,max_length=30, widget=forms.PasswordInput)



    def __init__(self, *args, **kwargs):

        super(RegisterOrganiserForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.label_class = 'col-lg-3 col-md-3 col-sm-12 control-label'
        self.helper.field_class = 'col-lg-9 col-md-9'
        self.helper.add_input(Submit('submit', 'Submit'))


    def clean(self):
        cleaned_data = super(RegisterOrganiserForm, self).clean()
        username = cleaned_data.get("username")
        password = cleaned_data.get("password")
        repeat_password = cleaned_data.get("rpt_password")

        for user in User.objects.all():
            if user.username ==username:
               raise forms.ValidationError("Username already in use")

        if password!=repeat_password:
            raise forms.ValidationError("Passwords do not match")


class RegisterCompetitorForm(forms.Form):

    username = forms.CharField(required=True,max_length=30)

    email = forms.EmailField(required=True,max_length=50 )


    firstname = forms.CharField(max_length=100)
    surname = forms.CharField(max_length=100)
    date_of_birth = forms.DateField(help_text="DD/MM/YYYY",input_formats=['%Y-%m-%d', '%d/%m/%Y','%d-%m-%Y'])
    gender = forms.ChoiceField(required=False, choices=FORM_GENDER_CHOICES, widget=forms.RadioSelect)

    club = forms.CharField(max_length=100, required=False)

    password = forms.CharField(required=True,max_length=30, widget=forms.PasswordInput)
    rpt_password = forms.CharField(required=True,max_length=30, widget=forms.PasswordInput)


    def __init__(self, *args, **kwargs):
        super(RegisterCompetitorForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = True

        self.helper.label_class = 'col-lg-3 col-md-3 col-sm-12 control-label'
        self.helper.field_class = 'col-lg-9 col-md-9'
        self.helper.add_input(Submit('submit', 'Submit'))


    def clean(self):
        cleaned_data = super(RegisterCompetitorForm, self).clean()
        username = cleaned_data.get("username")
        password = cleaned_data.get("password")
        repeat_password = cleaned_data.get("rpt_password")

        for user in User.objects.all():
            if user.username ==username:
               raise forms.ValidationError("Username already in use")

        if password!=repeat_password:
            raise forms.ValidationError("Passwords do not match")


class EditUserForm(forms.Form):

    #new_password = forms.CharField(required=True,max_length=30, widget=forms.PasswordInput)
    #repeat_new_password = forms.CharField(required=True,max_length=30, widget=forms.PasswordInput)
    email = forms.EmailField(required=True,max_length=50)
    first_name = forms.CharField(required=False,max_length=30)
    surname = forms.CharField(required=False,max_length=30)

    def __init__(self, *args, **kwargs):
        super(EditUserForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_class = 'form-horizontal group-border hover-stripped'
        self.helper.label_class = 'col-lg-2 col-md-2 col-sm-12 control-label'
        self.helper.field_class = 'col-lg-6 col-md-6'
        self.helper.add_input(Submit('submit', 'Submit'))

class ChangePasswordForm(forms.Form):

    old_password = forms.CharField(required=True,max_length=30, widget=forms.PasswordInput)
    new_password = forms.CharField(required=True,max_length=30, widget=forms.PasswordInput)
    repeat_new_password = forms.CharField(required=True,max_length=30, widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        super(ChangePasswordForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_class = 'form-horizontal group-border hover-stripped'
        self.helper.label_class = 'col-lg-2 col-md-2 col-sm-12 control-label'
        self.helper.field_class = 'col-lg-6 col-md-6'
        self.helper.add_input(Submit('submit', 'Submit'))

    def clean(self):
        cleaned_data = super(ChangePasswordForm, self).clean()
        form_new_password = cleaned_data.get("new_password")
        form_repeat_password = cleaned_data.get("repeat_new_password")


        if form_new_password!=form_repeat_password:
            raise forms.ValidationError("Passwords do not match")



class AddCreditForm(forms.Form):

    credit_to_add = forms.IntegerField(required=True, max_value=200, min_value=10, initial=10, label="Credit to add (min £10)")

    def __init__(self, *args, **kwargs):
        super(AddCreditForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_class = 'form-horizontal group-border hover-stripped'
        self.helper.label_class = 'col-lg-5 col-md-5 col-sm-12 control-label'
        self.helper.field_class = 'col-lg-3 col-md-3'
        self.helper.add_input(Submit('submit', 'Submit'))