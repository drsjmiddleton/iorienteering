from datetime import date, datetime, timedelta, time

from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator, RegexValidator
from django.core.exceptions import ValidationError
from decimal import Decimal
from django.utils.timezone import now


from iOrienteering.static_values import *
from website.custom_model_fields import SizeTypeRestrictedFileField

def get_expiry_date():
    return now() + USERMESSAGE_EXPIRY_TIME


# Create your models here.

class UserProfile(models.Model):
    GENDER = (
        ('M', 'M'),
        ('F', 'F'),
    )
    user= models.ForeignKey(User)
    credit = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    user_type = models.IntegerField(default=USER_TYPE_COMPETITOR)
    firstname = models.CharField(max_length=100)
    surname = models.CharField(max_length=100)
    date_of_birth = models.DateField(help_text="DD/MM/YYYY",blank=True, null=True)
    gender = models.CharField(max_length=1, choices=GENDER, blank=True)
    club = models.CharField(max_length=100, blank=True)
    subscribing_users=models.ManyToManyField("self", blank=True)


    def __str__(self):
        return self.user.username

    class Meta:
        unique_together = (("firstname", "surname", "date_of_birth"),)


class Area(models.Model):
    user = models.ForeignKey(User)
    area_name = models.CharField(max_length=200, unique=True)
    public = models.BooleanField(default=True)
    latitude = models.DecimalField(max_digits=9, decimal_places=6, default=Decimal(0.000))
    longitude = models.DecimalField(max_digits=9, decimal_places=6, default=Decimal(0.000))

    class Meta:
        ordering = ('-area_name',)
        unique_together = ()

    def __str__(self):
        return self.area_name+" "+self.user.username

class Checkpoint(models.Model):
    area = models.ForeignKey(Area)
    checkpoint_name = models.CharField(max_length=30, verbose_name="Control name")
    checkpoint_ID = models.CharField(verbose_name="Control ID",validators=[RegexValidator(regex='^[0-9A-F]{3}$', message='Requires 3 Hex characters (0-9 and A-F)', code='nomatch')], max_length=3, help_text="3 hex-characters (0-9 and A-F)  ie 111 or 3FB")
    is_start = models.BooleanField(default=False)
    is_finish = models.BooleanField(default=False)
    points_value = models.PositiveIntegerField(blank=True, default=0, help_text='If this checkpoint will be used on a score course',validators=[MaxValueValidator(9999), MinValueValidator(0)])

    class Meta:
        unique_together = (("area", "checkpoint_ID"),("area", "checkpoint_name"),)

    def __str__(self):
        return self.checkpoint_name


class Course(models.Model):
    area = models.ForeignKey(Area)
    course_name = models.CharField(max_length=30)
    course_description = models.CharField(max_length=500, blank=True, null=True)
    distance = models.DecimalField(max_digits=4, decimal_places=1, blank=True, null=True)
    allow_multiple_runs = models.BooleanField(default=False)
    is_score = models.BooleanField(default=False)

    time_limit = models.PositiveIntegerField(blank=True, default=60, validators=[MaxValueValidator(720), MinValueValidator(1)])
    penalty = models.FloatField(blank=True, default=0.2, validators=[MaxValueValidator(1000), MinValueValidator(0.001)])



    class Meta:
        unique_together = (("area", "course_name"),)

    def __str__(self):
        return self.area.area_name+" "+self.course_name

    def get_area_name(self):
        return self.area.area_name

    def get_course_codes(self):

        listOfCheckpoints = list(self.orderedcheckpoint_set.all().order_by('order_number'))

        course_codes = ''

        for cp in listOfCheckpoints:
            course_codes+=cp.checkpoint.checkpoint_ID

        return course_codes


        return self.course.checkpoint_order

    def get_course_points(self):
        if not self.is_score:
            return ''
        else:
            listOfCheckpoints = list(self.orderedcheckpoint_set.all().order_by('order_number'))
            course_points = ''

            for cp in listOfCheckpoints:
                course_points += (str(cp.checkpoint.points_value)+',')

            return course_points


class OrderedCheckpoint(models.Model):

    checkpoint = models.ForeignKey(Checkpoint)
    course = models.ForeignKey(Course)
    order_number = models.IntegerField()

def map_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    new_filename = instance.map_name+'_'+instance.area.area_name
    return 'user_{0}/{1}'.format(instance.user.id, new_filename)

class Map(models.Model):
    user = models.ForeignKey(User)
    area = models.ForeignKey(Area)
    map_name = models.CharField(max_length=30)
    map_image = SizeTypeRestrictedFileField(upload_to=map_directory_path, content_types=MAP_IMAGE_TYPES, max_upload_size=MAP_IMAGE_MAX_SIZE)
    cost = models.DecimalField(max_digits=10, decimal_places=2, default=1.00)

    class Meta:
        unique_together = (("area", "map_name"),)

    def __str__(self):
        return "%s (%s) - %s" % (self.map_name, self.area.area_name, self.area.user.username)


class CourseCompetitor(models.Model):
    area = models.ForeignKey(Area)
    organiser = models.ForeignKey(UserProfile, related_name='course_organiser')
    competitor = models.ForeignKey(UserProfile, related_name='parent_competitor')
    course = models.ForeignKey(Course, blank=False, null=True, on_delete=models.SET_NULL)
    entered_date = models.DateField(blank=True, null=True)
    run_date = models.DateField(blank=True, null=True)

    def hasCompeted(self):
        if self.checkpointtime_set.all().get('checkpoint__is_start').time:
            return True
        else:
            return False


    def __str__(self):
        return "%s %s - %s" % (self.competitor.user_profile.firstname, self.competitor.user_profile.surname, self.area.course_name)

class Result(models.Model):
    competitor = models.ForeignKey(UserProfile, related_name='result_competitor')
    organiser = models.ForeignKey(UserProfile, related_name='result_organiser')
    run_date = models.DateField(blank=True, null=True)
    splits_xml = models.TextField()
    status = models.IntegerField(default=0, validators=[MaxValueValidator(4), MinValueValidator(0)])
    course = models.ForeignKey(Course)
    area = models.ForeignKey(Area)
    points = models.IntegerField(default=0)
    run_time = models.IntegerField(default=0)
    dsq_reason = models.CharField(blank=True, null=True, max_length=100)

    class Meta:
        unique_together = (("competitor","run_date","course","run_time"),)


class WorkingResult():

    def __init__(self, database_result):
        self.firstname = database_result.competitor.firstname
        self.surname = database_result.competitor.surname
        self.age_gender_class = self.getAgeClass(database_result)
        self.club = database_result.competitor.club

    def parseXML(self):
        course = self.database_result.course


    def getAgeClass(database_result):
        effective_age_years = database_result.run_date.year - database_result.competitor.userprofile.date_of_birth.year

        if effective_age_years < 11:
            effective_age_years = str(10)
        elif effective_age_years < 21:
            effective_age_years = str(((effective_age_years + 1) // 2) * 2)
        elif effective_age_years > 79:
            effective_age_years = str(80)
        elif effective_age_years > 35:
            effective_age_years = str((effective_age_years // 5) * 5)
        else:
            effective_age_years = str(21)


        return database_result.competitor.gender+effective_age_years


class UserMessage(models.Model):

# to do  - insert preferences here

    message_title = models.CharField(blank=False, max_length=50)
    paypal_invoice_id = models.CharField(blank=True, max_length=100)
    message_text = models.TextField(blank=False)
    message_date = models.DateField(default=now)
    expire_date = models.DateField(default=get_expiry_date)
    important = models.BooleanField(default=False)
    universal_message = models.BooleanField(default=False)
    user = models.ForeignKey(User, null=True, blank=True)


