__author__ = 'drsjmiddleton'


from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, JsonResponse
from django.core.urlresolvers import reverse
from django.core import serializers
from django.contrib import messages
from django.contrib.messages import get_messages
from django.contrib.auth.decorators import login_required

from iOrienteering.static_values import *

from paypal.standard.forms import PayPalPaymentsForm
from paypal.standard.models import ST_PP_COMPLETED
from paypal.standard.ipn.signals import valid_ipn_received, invalid_ipn_received

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.files.storage import get_storage_class
from pathlib import Path

from website.models import *
from website.website_forms import *
from website.website_helper_functions import *
from django.contrib.auth import authenticate, login, logout
from django.conf import settings

from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes


def landing(request):

    context = {}
    return render(request, 'public/landing.html', context)

def t_and_c(request):

    static_storage = get_storage_class(settings.STATICFILES_STORAGE)()
    static_storage_path = Path(static_storage.base_location)

    path_to_pdf = str(static_storage_path / 'website/assets/docs/terms_of_website_use.pdf')

    with open(path_to_pdf, 'rb') as pdf:
        response = HttpResponse(pdf.read(),content_type='application/pdf')
        response['Content-Disposition'] = 'filename=terms_of_website_use.pdf'
        return response
    pdf.closed



def logout_user(request):

    logout(request)
    return HttpResponseRedirect(reverse('login_user'))



def login_user(request):

    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        loginForm = LoginForm(request.POST)


        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)

        if user is not None:
            # the password verified for the user
            if user.is_active:
                user_profile= get_object_or_404(UserProfile, user=user)
                login(request, user)

                # Token.objects.get_or_create(user=user)

                if user_profile.user_type==USER_TYPE_COMPETITOR:
                    return HttpResponseRedirect(reverse('competitor_home'))
                else:
                    return HttpResponseRedirect(reverse('organiser_home'))


            else:
                loginForm.add_error('username', 'The password is valid, but the account has been disabled')

        else:
            loginForm.add_error('password', 'The username and password combination is invalid')


    # if a GET (or any other method) we'll create a blank form
    else:
        loginForm = LoginForm()

    loginForm.helper.form_action = reverse('login_user')

    context = {'loginForm':loginForm}

    return render(request, 'public/registration_login/login.html', context)

def register_user_organiser(request):

        # check whether it's valid:
    if request.method == 'POST':

        register_form = RegisterOrganiserForm(data=request.POST)

        if register_form.is_valid():

            username = request.POST['username']
            password = request.POST['password']
            email = request.POST['email']
            firstname = request.POST['firstname']
            surname = request.POST['surname']

            User.objects.create_user(username, email, password)

            user = authenticate(username=username, password=password)

            if user is not None:
                Token.objects.get_or_create(user=user)
                UserProfile.objects.get_or_create(user=user, user_type=USER_TYPE_ORGANISER, firstname=firstname, surname=surname)

                login(request, user)
                return HttpResponseRedirect(reverse('organiser_home'))

    else:
        register_form = RegisterOrganiserForm()

    register_form.helper.form_action = reverse('register_user_organiser')


    context = {'register_form':register_form}

    return render(request, 'public/registration_login/register_organiser.html', context)

def register_user_competitor(request):

        # check whether it's valid:
    if request.method == 'POST':


        register_form = RegisterCompetitorForm(data=request.POST)

        if register_form.is_valid():

            username = register_form.cleaned_data['username']
            password = register_form.cleaned_data['password']
            email = register_form.cleaned_data['email']


            User.objects.create_user(username, email, password)

            user = authenticate(username=username, password=password)

            if user is not None:
                Token.objects.get_or_create(user=user)
                UserProfile.objects.get_or_create(user=user,
                                                  user_type=USER_TYPE_COMPETITOR,
                                                  firstname=register_form.cleaned_data['firstname'],
                                                  surname=register_form.cleaned_data['surname'],
                                                  date_of_birth=register_form.cleaned_data['date_of_birth'],
                                                  gender=register_form.cleaned_data['gender'],
                                                  club=register_form.cleaned_data['club'],
                                                  )

                login(request, user)
                return HttpResponseRedirect(reverse('competitor_home'))

    # if a GET (or any other method) we'll create a blank form
    else:
        register_form = RegisterCompetitorForm()


    register_form.helper.form_action = reverse('register_user_competitor')

    context = {'register_form':register_form}

    return render(request, 'public/registration_login/register_competitor.html', context)

#Views from user root




@login_required
def edit_profile(request):

    this_user = request.user

    # check whether it's valid:
    if request.method == 'POST':
        editUserForm = EditUserForm(request.POST)

        if editUserForm.is_valid():

            email = request.POST['email']
            first_name = request.POST['first_name']
            surname = request.POST['surname']

            #this_user.set_password(password)

            this_user.email = email
            this_user.first_name = first_name
            this_user.last_name = surname

            this_user.save()
            return HttpResponseRedirect(reverse('home'))


    # if a GET (or any other method) we'll create a blank form
    else:
        editUserForm = EditUserForm(initial={'email': this_user.email, 'first_name': this_user.first_name, 'surname': this_user.last_name})

    editUserForm.helper.form_action = reverse('edit_profile')

    context = {'editUserForm':editUserForm, 'this_user': this_user}

    return render(request, 'website/profile.html', context)

@login_required
def change_password(request):

    this_user = request.user

    error_message = None

    # check whether it's valid:
    if request.method == 'POST':
        changePasswordForm = ChangePasswordForm(request.POST)

        if changePasswordForm.is_valid():

            old_password = request.POST['old_password']
            new_password = request.POST['new_password']

            user = authenticate(username=this_user.username, password=old_password)

            if user is not None:

                this_user.set_password(new_password)

                this_user.save()

                this_user = authenticate(username=this_user.username, password=new_password)

                login(request, this_user)

                messages.info(request, 'Your password has been successfully changed.')

                return HttpResponseRedirect(reverse('home'))

            else:
                messages.error(request, True)

    # if a GET (or any other method) we'll create a blank form

    changePasswordForm = ChangePasswordForm()

    changePasswordForm.helper.form_action = reverse('change_password')

    storage = get_messages(request)
    for message in storage:
        error_message=message

    context = {'changePasswordForm':changePasswordForm, 'this_user': this_user, 'error_message':error_message}

    return render(request, 'website/change_password.html', context)




# payment views

@login_required
def credit(request):
    this_user = request.user

    this_user_profile = get_object_or_404(UserProfile, user=this_user)

    this_user_profile.display_credit="{0:.2f}".format(this_user_profile.credit)

    credit_form = AddCreditForm()

    credit_form.helper.form_action = reverse('credit_confirm')

    context = {'this_user':this_user , 'credit_form':credit_form ,'this_user_profile':this_user_profile }

    return render(request, 'website/credit.html', context)

@login_required
def credit_confirm(request):
    this_user = request.user

    this_user_profile = get_object_or_404(UserProfile, user=this_user)

    credit_to_add="0.00"

    if request.method == 'POST':
        credit_to_add = request.POST["credit_to_add"]
        unique_invoice_id = str(this_user.id)+'_'+str(unix_time(datetime.now()))

        paypal_dict = {
            "business": settings.PAYPAL_RECEIVER_EMAIL,
            "amount": credit_to_add,
            "currency_code":"GBP",
            "item_name": "iOrienteering Credit",
            "invoice": unique_invoice_id,
            "notify_url": "http://www.racetek-live.co.uk" + reverse('paypal-ipn'),
            "return_url": "http://www.racetek-live.co.uk/website/credit/payment_made/?unique_invoice_id="+unique_invoice_id,
            "cancel_return": "http://www.racetek-live.co.uk/website/credit/payment_cancelled/",

        }

    # Create the instance.
        form = PayPalPaymentsForm(initial=paypal_dict)

        context = {"form": form, 'this_user':this_user,'this_user_profile':this_user_profile, 'credit_to_add':credit_to_add}
        return render(request, "website/credit_confirm.html", context)


@csrf_exempt
@login_required
def payment_made(request):
    this_user = request.user
    unique_invoice_id=request.GET["unique_invoice_id"]

    ipn_already_received = False

    messages = this_user.usermessage_set.all()

    message_expire_time = timedelta(days=2)

    waiting_message = "Your recent payment has been made and we are awaiting confirmation of its success from Paypal"

    for message in messages:
        if message.paypal_invoice_id==unique_invoice_id:
            ipn_already_received = True

    if not ipn_already_received:
        awaiting_payment = UserMessage(message_title="Payment Confirmation Awaited",paypal_invoice_id= unique_invoice_id, message_text=waiting_message, message_date=date.today(), expire_date=date.today()+message_expire_time, important=True, user=this_user )
        awaiting_payment.save()

    return HttpResponseRedirect(reverse('home'))

@csrf_exempt
@login_required
def payment_cancelled(request):
    this_user = request.user

    context = {"this_user": this_user}
    return render(request, "website/payment_cancelled.html", context)

@api_view(['POST'])
@csrf_exempt
def api_competitor_get_token(request):

    request.data['username']
    request.data['password']

    user = authenticate(username=request.data['username'], password=request.data['password'])

    if user is not None:
        this_user_profile = get_object_or_404(UserProfile, user=user)

        if this_user_profile.user_type!=USER_TYPE_COMPETITOR:
            return Response(status=status.HTTP_403_FORBIDDEN)

        token=Token.objects.get_or_create(user=user)[0]
        token_str = token.key

        return JsonResponse(({'token': token_str}))

    return Response(status=status.HTTP_403_FORBIDDEN)



