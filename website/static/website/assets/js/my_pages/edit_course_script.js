/**
 * Created by drsjmiddleton on 19/08/2016.
 */

/**
 * Created by drsjmiddleton on 22/01/2015.
 */

var local_dictionary_of_course_ID_type = dictionary_of_course_ID_type;
var local_dictionary_of_course_ID_name = dictionary_of_course_ID_name;

var list_of_cp_on_course=[];






$(document).ready( function () {

    //document.getElementById('checkpoint_order_list');

    var listOfCheckpointsButtons = document.getElementById("checkpoint_order_list");


    var sortable = Sortable.create(listOfCheckpointsButtons, {
		animation: 150,
		filter: '.js-remove',
		onFilter: function (evt) {

            var index = list_of_cp_on_course.indexOf(evt.item.id);
            list_of_cp_on_course.splice(index, 1);
            updateCounters();

			evt.item.parentNode.removeChild(evt.item);

		}
	});

    if (CHECKPOINT_ORDER_STRING){

        

    for(var i=0;i<(CHECKPOINT_ORDER_STRING.length)/3;i++) {


        var new_strin = (CHECKPOINT_ORDER_STRING.slice(i*3,(i*3)+3));
        list_of_cp_on_course.push(CHECKPOINT_ORDER_STRING.slice(i*3,(i*3)+3));

    }
}

    updateCounters();
    
    updateCheckpointCourseList();


    $('.cp_avail_button').on('click',function()
    {

        var this_button = $(this);

        var cp_name = this_button.attr('id')

        list_of_cp_on_course.push($(this).attr('id'))

        updateCheckpointCourseList();

        updateCounters();

        return;


    });

    $( "#course_form" ).submit(function() {

        var cp_list_items = $('#checkpoint_order_list').children();

        var list_length = cp_list_items.length

        var returnString="";

        for(var i=0;i<list_length;i++){

            var this_item = cp_list_items[i].id;
            returnString += cp_list_items[i].id;

        }

        var temp = null;

        $( "#id_checkpoint_order" ).val(returnString);

        return


    });


    change_for_score($("#id_is_score").is(':checked'))




    $('#id_is_score').on('ifChanged', function(event){

							change_for_score($("#id_is_score").is(':checked'))
                    } );
} );



function change_for_score(is_score){


    if(is_score){

        //$("#Checkpoints_label")[0].innerHTML = "Checkpoints <span class=\"asteriskField\">*</span>";
        
        $("#div_id_time_limit").show();

        $("#div_id_penalty").show();




    }

    else{

        
        $("#div_id_time_limit").hide();

        $("#div_id_penalty").hide();

        //$("#Checkpoints_label")[0].innerHTML = "Checkpoint Order <span class=\"asteriskField\">*</span>";



    }


}

function updateCheckpointCourseList(){


    $( '#checkpoint_order_list' ).empty();

        for (var i = 0; i < list_of_cp_on_course.length; i++) {


            var type = local_dictionary_of_course_ID_type[list_of_cp_on_course[i]];

            switch (type) {

                case 1:


                    $('#checkpoint_order_list').append("<li id=\'"+list_of_cp_on_course[i]+"\'><span class=\"label label-success mr10 mb10\">"+local_dictionary_of_course_ID_name[list_of_cp_on_course[i]]+"</span><span class=\"js-remove\"><i class=\"fa-remove color-gray\"></i></span></li>");

                    break;
                case 2:
                    $('#checkpoint_order_list').append("<li id=\'"+list_of_cp_on_course[i]+"\'><span class=\"label label-danger mr10 mb10\">"+local_dictionary_of_course_ID_name[list_of_cp_on_course[i]]+"</span><span class=\"js-remove\"><i class=\"fa-remove color-gray\"></i></span></li>");

                    break;

                default:
                    $('#checkpoint_order_list').append("<li id=\'"+list_of_cp_on_course[i]+"\'><span class=\"label label-info mr10 mb10\">"+local_dictionary_of_course_ID_name[list_of_cp_on_course[i]]+"</span><span class=\"js-remove\"><i class=\"fa-remove color-gray\"></i></span></li>");
                    break

            }


        }



}

function updateCounters(){

    var temp_array =countUniqueInArray(list_of_cp_on_course);

    var checkpoint_ids=temp_array[0];
    var checkpoint_counters = temp_array[1];

    for (k in local_dictionary_of_course_ID_name) if (local_dictionary_of_course_ID_name.hasOwnProperty(k)){

        var find_string = 'div[counter-id='+k+']'

        $(find_string).text("0");


    }

    for (var i = 0; i < checkpoint_ids.length; i++) {

         var find_string = 'div[counter-id='+checkpoint_ids[i]+']'

        $(find_string).text(checkpoint_counters[i]);


    }


}

function countUniqueInArray(arr) {
    var a = [], b = [], prev;

    var temp_array= arr.slice();
    
    temp_array.sort();
    for ( var i = 0; i < temp_array.length; i++ ) {
        if ( temp_array[i] !== prev ) {
            a.push(temp_array[i]);
            b.push(1);
        } else {
            b[b.length-1]++;
        }
        prev = temp_array[i];
    }

    return [a, b];
}

 /*for (var i = 0; i < select_box_checkpoints.selectedOptions.length; i++) {
//selectedOptions[]
            var label = select_box_checkpoints.selectedOptions[i].label;

            var type = local_dictionary_of_course_name_type[label];

            switch (type) {

                case 1:
                    $('#checkpoint_order_list').append("<li><button type=\"button\" class=\"btn btn-warning\">"+label+"</button></li>");

                    break;
                case 2:
                    $('#checkpoint_order_list').append("<li><button type=\"button\" class=\"btn btn-success\">"+label+"</button></li>");

                    break;
                case 3:
                    $('#checkpoint_order_list').append("<li><button type=\"button\" class=\"btn btn-danger\">"+label+"</button></li>");

                    break;
                default:
                    $('#checkpoint_order_list').append("<li><button type=\"button\" class=\"btn btn-info\">"+label+"</button></li>");
                    break

            }


        }*/