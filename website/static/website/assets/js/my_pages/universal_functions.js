/**
 * Created by drsjmiddleton on 23/07/2016.
 */

function secondsToTimeString(seconds){

                var hours = Math.floor(seconds/3600);
                var minutes = Math.floor((seconds - (hours * 3600)) / 60);
                var mSeconds = Math.floor(seconds - (hours * 3600) - (minutes * 60));


                var hours_string = hours<10 ? "0" + hours : hours + "";
                var minutes_string = minutes<10 ? "0" + minutes : minutes + "";
                var seconds_string = mSeconds<10 ? "0" + mSeconds : mSeconds + "";
    
    
    if(hours>0){
        
        return hours_string+":"+minutes_string+":"+seconds_string;
    }
    
    else{
        
        return minutes_string+":"+seconds_string;
    }
    

}