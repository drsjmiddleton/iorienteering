/**
 * Created by drsjmiddleton on 17/03/2015.
 */


//Global

var LOCAL_AREA_PK = AREA_PK;

//standard table globals

var STANDARD_TRIGGER_OBJECT = {};

var STANDARD_LOCAL_JSON_COMPETITORS_LIST;

var STANDARD_FILTER_DETAILS_ARRAY = {age: [], gender: [], course: []};

var STANDARD_NAME_FOR_TITLE = AREA_NAME + " Results";

var STANDARD_AJAX_IN_PROGRESS=false;

var STANDARD_VIEW_SPLITS_ROW;

var STANDARD_TABLE_INITIALISED = false;



//score table variables

var SCORE_TRIGGER_OBJECT = {};

var SCORE_LOCAL_JSON_COMPETITORS_LIST;

var SCORE_FILTER_DETAILS_ARRAY = {age: [], gender: [], course: []};

var SCORE_NAME_FOR_TITLE = AREA_NAME + " Score Results";

var SCORE_AJAX_IN_PROGRESS=false;

var SCORE_VIEW_SPLITS_ROW;

var SCORE_TABLE_INITIALISED = false;




function currentStandardResultsMessage() {

    if ((STANDARD_FILTER_DETAILS_ARRAY.age.length + STANDARD_FILTER_DETAILS_ARRAY.gender.length + STANDARD_FILTER_DETAILS_ARRAY.course.length) == 0) {

        return "";
    }

    var filter_string_array = ["[", "[", "["]

    for (var i = 0; i < STANDARD_FILTER_DETAILS_ARRAY.age.length; i++) {


        filter_string_array[2] += STANDARD_FILTER_DETAILS_ARRAY.age[i]+", ";
    }

    for (var i = 0; i < STANDARD_FILTER_DETAILS_ARRAY.gender.length; i++) {


        filter_string_array[1] += STANDARD_FILTER_DETAILS_ARRAY.gender[i]+", ";
    }
    for (var i = 0; i < STANDARD_FILTER_DETAILS_ARRAY.course.length; i++) {


        filter_string_array[0] += STANDARD_FILTER_DETAILS_ARRAY.course[i]+", ";
    }


    var return_string = "Filtered on: ";

    for (var i = 0; i < filter_string_array.length; i++) {

        if (filter_string_array[i].length > 1) {

            filter_string_array[i] = filter_string_array[i].slice(0, -2);
            filter_string_array[i] += "]";
            return_string += filter_string_array[i];
        }


    }

    if(return_string==="Filtered on: "){

        return "";
    }

    return return_string;


}

function filterOnStandardCourse(){



    var search_string = $("#id_standard_course_filter option:selected").text();;


        $('#standard_results_table').DataTable().column(6).search(search_string ? '^(' + search_string + ')$' : '', true, false)
            .draw();

        STANDARD_FILTER_DETAILS_ARRAY.course.length = 0

        STANDARD_FILTER_DETAILS_ARRAY.course.push(search_string);

        //Object used to update the call from the pdf callback

        $(STANDARD_TRIGGER_OBJECT).trigger("trigger");


}

function filterOnScoreCourse(){



    var search_string = $("#id_standard_course_filter option:selected").text();;


        $('#standard_results_table').DataTable().column(6).search(search_string ? '^(' + search_string + ')$' : '', true, false)
            .draw();

        STANDARD_FILTER_DETAILS_ARRAY.course.length = 0

        STANDARD_FILTER_DETAILS_ARRAY.course.push(search_string);

        //Object used to update the call from the pdf callback

        $(STANDARD_TRIGGER_OBJECT).trigger("trigger");


}

function getAjaxStandardResults() {

    $('#standard_results_table').dataTable().api().ajax.reload(function (json) {

        STANDARD_LOCAL_JSON_COMPETITORS_LIST = JSON.parse(JSON.stringify(json));
        STANDARD_AJAX_IN_PROGRESS=false;

    }, false);


}

function initStandardResultsTable() {


    $('#standard_results_table').dataTable({


        "initComplete": function(settings, json) {
    STANDARD_TABLE_INITIALISED=true;
            filterOnStandardCourse();
  },

        "ajax": {
            
            "url": '/website/organiser/areas/' + LOCAL_AREA_PK + '/ajax_organiser_area_results/',
            "dataSrc": function (json) {

                STANDARD_LOCAL_JSON_COMPETITORS_LIST = JSON.parse(JSON.stringify(json));
                return JSON.parse(JSON.stringify(json));

            }
        },


        "columns": [

            {
                "title": "Position", "data": null, "width": "7%",

                "render": function (data, type, row, meta) {

                    var rowIndex = meta.row;

                    if (parseInt(row[6]) === "NaN") {

                        return '#';
                    }

                    if (rowIndex > 0) {

                        var thisTableApi = $('#standard_results_table').dataTable().api();

                        var lastRowData = thisTableApi.row(rowIndex - 1).data();

                        if (row[6]===lastRowData[6]) {

                            var last_unique_position = thisTableApi.row(rowIndex - 1).node().children[0].innerHTML;

                            last_unique_position = last_unique_position.replace(/\D/g, '');

                            thisTableApi.row(rowIndex - 1).node().children[0].innerHTML = '=' + last_unique_position;

                            return '=' + last_unique_position;

                        }

                    }


                    var position = meta.row + 1;

                    return position;

                }
            },
            {
                "title": "First name", "width": "15%",

                "data": function (row, type, val, meta) {

                    return row[0];

                }
            },
            {
                "title": "Surname", "width": "15%",

                "data": function (row, type, val, meta) {

                    return row[1];

                }
            },
            {

                //hidden gender column
                "visible": false,
                "searchable": true,"width": "0%",

                "data": function (row, type, val, meta) {

                    return row[3];

                }
            },
            {
                "title": "Age Cat.", "width": "10%",

                "data": function (row, type, val, meta) {

                    return row[3];

                }
            },
            {
                "title": "Club", "width": "13%",

                "data": function (row, type, val, meta) {

                    return row[5];

                }
            },
            {
                "title": "Course", "width": "10%",

                "data": function (row, type, val, meta) {

                    return row[4];

                }
            },
            
            {
                "title": "Time", "width": "10%",

                "data": function (row, type, val, meta) {

                    var int_seconds = parseInt(row[6]);

                    if(int_seconds=="NaN"){
                        
                        return row[6];
                    }
                    
                    else{

                        var string_time = secondsToTimeString(int_seconds);
                        
                        return secondsToTimeString(int_seconds);
                    }

                }
            },

        {"width": "5%","data": null,
            "render": function ( data, type, row, meta ) {

				return '<button type="button" id="view_edit_splits" class="btn btn-xs btn-warning" data-toggle="modal" data-rowid="'+meta.row+'" data-compid="'+row[11]+'" data-target="#viewEditSplitModal">Splits</button>';}
		},


        ],

        "language": {
            "search": "_INPUT_",
            "searchPlaceholder": "Filter Results",
            "info": "Showing _START_ to _END_ of _TOTAL_ results"
        },
        "ordering": false,

        "lengthMenu": [[-1,25, 50, 100], ["All", 25, 50, 100]],


        "dom": "<'row'<'col-lg-3 col-md-3 col-sm-12 text-center'l><'col-lg-6 col-md-6 col-sm-12 text-center'B><'col-lg-3 col-md-3 col-sm-12 text-center'f>r>t<'row-'<'col-lg-6 col-md-6 col-sm-12'i><'col-lg-6 col-md-6 col-sm-12'p>>",
        "buttons": [
            {
                extend: "print",
                init: function(dt, node, config) {

                    $(STANDARD_TRIGGER_OBJECT).bind("trigger", function(){

                         config.message = currentStandardResultsMessage();
                    });


    },
                exportOptions: {
                    stripHtml: false,
                    stripNewlines: false,
                    decodeEntities: true,

                    format: {
                        body: function (data, columnIdx, rowIdx) {

                            if (columnIdx === 0) {

                                return $('#standard_results_table tbody')[0].children[rowIdx].children[0].innerHTML;
                            }


                            var return_string = String(data).replace(/<br\s*\/?>/mg, "\n");

                            return return_string;

                        }
                    }
                },
                orientation: "landscape",
                "title": STANDARD_NAME_FOR_TITLE,
                text: '<i class="st-printer s16 vat"></i> Print'


            },
            {
                "extend": "excel",
                init: function(dt, node, config) {

                    $(STANDARD_TRIGGER_OBJECT).bind("trigger", function(){

                         config.sheetname = currentStandardResultsMessage();
                    });


    },
                exportOptions: {
                    stripHtml: false,
                    stripNewlines: false,
                    decodeEntities: true,

                    format: {
                        body: function (data, columnIdx, rowIdx) {

                            if (columnIdx === 0) {

                                return $('#standard_results_table tbody')[0].children[rowIdx].children[0].innerHTML;
                            }

                            var return_string = String(data).replace(/<br\s*\/?>/mg, "\n");

                            return return_string;

                        }
                    }
                },



                "title": STANDARD_NAME_FOR_TITLE,
                "text": '<i class="im-file-excel s16 vat"></i> XLS'
            },
            {
                extend: "pdf",
                init: function(dt, node, config) {

                    $(STANDARD_TRIGGER_OBJECT).bind("trigger", function(){

                         config.message = currentStandardResultsMessage();
                    });


    },
                exportOptions: {
                    stripHtml: false,
                    stripNewlines: false,
                    decodeEntities: true,

                    format: {
                        body: function (data, columnIdx, rowIdx) {

                            if (columnIdx === 0) {

                                return $('#standard_results_table tbody')[0].children[rowIdx].children[0].innerHTML;
                            }

                            var return_string = String(data).replace(/<br\s*\/?>/mg, "\n");

                            return return_string;

                        }
                    }
                },
                orientation: "landscape",
                title: STANDARD_NAME_FOR_TITLE,
                text: '<i class="im-file-pdf s16 vat"></i> PDF'
            },
            {
                "extend": "csv",
                exportOptions: {
                    stripHtml: false,
                    stripNewlines: false,
                    decodeEntities: true,
                    format: {
                        body: function (data, columnIdx, rowIdx) {

                            if (columnIdx === 0) {

                                return $('#standard_results_table tbody')[0].children[rowIdx].children[0].innerHTML;
                            }

                            var return_string = String(data).replace(/<br\s*\/?>/mg, "\n");

                            return return_string;

                        }
                    }
                },

                "title": STANDARD_NAME_FOR_TITLE,
                "text": '<i class="im-file-xml s16 vat"></i> CSV'
            },
            {
                "extend": "copy",
                exportOptions: {
                    stripHtml: false,
                    stripNewlines: false,
                    decodeEntities: true

                },

                "title": STANDARD_NAME_FOR_TITLE,
                "text": '<i class="im-copy s16 vat"></i> Copy'
            }
        ]


    });


}

function initScoreResultsTable() {


    $('#score_results_table').dataTable({


        "initComplete": function(settings, json) {
    SCORE_TABLE_INITIALISED=true;
            filterOnScoreCourse();
  },

        "ajax": {

            "url": '/website/organiser/areas/' + LOCAL_AREA_PK + '/ajax_organiser_area_score_results/',
            "dataSrc": function (json) {

                SCORE_LOCAL_JSON_COMPETITORS_LIST = JSON.parse(JSON.stringify(json));
                return JSON.parse(JSON.stringify(json));

            }
        },


        "columns": [

            {
                "title": "Position", "data": null, "width": "7%",

                "render": function (data, type, row, meta) {

                    var rowIndex = meta.row;

                    if (parseInt(row[6]) === "NaN") {

                        return '#';
                    }

                    if (rowIndex > 0) {

                        var thisTableApi = $('#score_results_table').dataTable().api();

                        var lastRowData = thisTableApi.row(rowIndex - 1).data();

                        if (row[6]===lastRowData[6]) {

                            var last_unique_position = thisTableApi.row(rowIndex - 1).node().children[0].innerHTML;

                            last_unique_position = last_unique_position.replace(/\D/g, '');

                            thisTableApi.row(rowIndex - 1).node().children[0].innerHTML = '=' + last_unique_position;

                            return '=' + last_unique_position;

                        }

                    }


                    var position = meta.row + 1;

                    return position;

                }
            },
            {
                "title": "First name", "width": "15%",

                "data": function (row, type, val, meta) {

                    return row[0];

                }
            },
            {
                "title": "Surname", "width": "15%",

                "data": function (row, type, val, meta) {

                    return row[1];

                }
            },
            {

                //hidden gender column
                "visible": false,
                "searchable": true,"width": "0%",

                "data": function (row, type, val, meta) {

                    return row[3];

                }
            },
            {
                "title": "Age Cat.", "width": "10%",

                "data": function (row, type, val, meta) {

                    return row[3];

                }
            },
            {
                "title": "Club", "width": "13%",

                "data": function (row, type, val, meta) {

                    return row[5];

                }
            },
            {
                "title": "Course", "width": "10%",

                "data": function (row, type, val, meta) {

                    return row[4];

                }
            },

            {
                "title": "Points", "width": "10%",

                "data": function (row, type, val, meta) {

                    var int_seconds = parseInt(row[6]);

                    if(int_seconds=="NaN"){

                        return row[6];
                    }

                    else{

                        return row[8];
                    }

                }
            },


        ],

        "language": {
            "search": "_INPUT_",
            "searchPlaceholder": "Filter Results",
            "info": "Showing _START_ to _END_ of _TOTAL_ results"
        },
        "ordering": false,

        "lengthMenu": [[-1,25, 50, 100], ["All", 25, 50, 100]],


        "dom": "<'row'<'col-lg-3 col-md-3 col-sm-12 text-center'l><'col-lg-6 col-md-6 col-sm-12 text-center'B><'col-lg-3 col-md-3 col-sm-12 text-center'f>r>t<'row-'<'col-lg-6 col-md-6 col-sm-12'i><'col-lg-6 col-md-6 col-sm-12'p>>",
        "buttons": [
            {
                extend: "print",
                init: function(dt, node, config) {

                    $(STANDARD_TRIGGER_OBJECT).bind("trigger", function(){

                         config.message = currentStandardResultsMessage();
                    });


    },
                exportOptions: {
                    stripHtml: false,
                    stripNewlines: false,
                    decodeEntities: true,

                    format: {
                        body: function (data, columnIdx, rowIdx) {

                            if (columnIdx === 0) {

                                return $('#standard_results_table tbody')[0].children[rowIdx].children[0].innerHTML;
                            }


                            var return_string = String(data).replace(/<br\s*\/?>/mg, "\n");

                            return return_string;

                        }
                    }
                },
                orientation: "landscape",
                "title": STANDARD_NAME_FOR_TITLE,
                text: '<i class="st-printer s16 vat"></i> Print'


            },
            {
                "extend": "excel",
                init: function(dt, node, config) {

                    $(STANDARD_TRIGGER_OBJECT).bind("trigger", function(){

                         config.sheetname = currentStandardResultsMessage();
                    });


    },
                exportOptions: {
                    stripHtml: false,
                    stripNewlines: false,
                    decodeEntities: true,

                    format: {
                        body: function (data, columnIdx, rowIdx) {

                            if (columnIdx === 0) {

                                return $('#standard_results_table tbody')[0].children[rowIdx].children[0].innerHTML;
                            }

                            var return_string = String(data).replace(/<br\s*\/?>/mg, "\n");

                            return return_string;

                        }
                    }
                },



                "title": STANDARD_NAME_FOR_TITLE,
                "text": '<i class="im-file-excel s16 vat"></i> XLS'
            },
            {
                extend: "pdf",
                init: function(dt, node, config) {

                    $(STANDARD_TRIGGER_OBJECT).bind("trigger", function(){

                         config.message = currentStandardResultsMessage();
                    });


    },
                exportOptions: {
                    stripHtml: false,
                    stripNewlines: false,
                    decodeEntities: true,

                    format: {
                        body: function (data, columnIdx, rowIdx) {

                            if (columnIdx === 0) {

                                return $('#standard_results_table tbody')[0].children[rowIdx].children[0].innerHTML;
                            }

                            var return_string = String(data).replace(/<br\s*\/?>/mg, "\n");

                            return return_string;

                        }
                    }
                },
                orientation: "landscape",
                title: STANDARD_NAME_FOR_TITLE,
                text: '<i class="im-file-pdf s16 vat"></i> PDF'
            },
            {
                "extend": "csv",
                exportOptions: {
                    stripHtml: false,
                    stripNewlines: false,
                    decodeEntities: true,
                    format: {
                        body: function (data, columnIdx, rowIdx) {

                            if (columnIdx === 0) {

                                return $('#standard_results_table tbody')[0].children[rowIdx].children[0].innerHTML;
                            }

                            var return_string = String(data).replace(/<br\s*\/?>/mg, "\n");

                            return return_string;

                        }
                    }
                },

                "title": STANDARD_NAME_FOR_TITLE,
                "text": '<i class="im-file-xml s16 vat"></i> CSV'
            },
            {
                "extend": "copy",
                exportOptions: {
                    stripHtml: false,
                    stripNewlines: false,
                    decodeEntities: true

                },

                "title": STANDARD_NAME_FOR_TITLE,
                "text": '<i class="im-copy s16 vat"></i> Copy'
            }
        ]


    });


}

$(document).ready(function () {

    /*$.getScript(STATIC_URL_FOR_UNIVERSAL_FUNCTIONS, function(){

   toggle_credit_display(stringToBoolean(NEEDS_MORE_CREDIT));

    check_credit_status(RACE_PK);

    });*/

    initStandardResultsTable();

    initScoreResultsTable();

    //standard filter callbacks

    $('#id_standard_age_filter').on('change', function () {

        if(!STANDARD_TABLE_INITIALISED){

            return;
        }

        var search_string = "";
        var selected_array = [];

        for (var i = 0; i < this.children.length; i++) {

            if (this.children[i].selected) {

                selected_array.push(this.children[i].innerHTML);
            }

        }

        for (var i = 0; i < selected_array.length; i++) {

            search_string = search_string + '|' + $.fn.dataTable.util.escapeRegex(selected_array[i]);

        }

        $('#standard_results_table').DataTable().column(3).search(search_string ? '^(' + search_string + ')$' : '', true, false)
            .draw();

        STANDARD_FILTER_DETAILS_ARRAY.age.length = 0

        for (var i = 0; i < selected_array.length; i++) {
            STANDARD_FILTER_DETAILS_ARRAY.age.push(selected_array[i]);
        }

        $(STANDARD_TRIGGER_OBJECT).trigger("trigger");

    });

    $('#id_standard_course_filter').on('change', function () {


        if(!STANDARD_TABLE_INITIALISED){

            return;
        }

        filterOnStandardCourse();

    });

    $('#id_standard_gender_filter').on('change', function () {


        if(!STANDARD_TABLE_INITIALISED){

            return;
        }

        var search_string = "";
        var selected_array = [];

        for (var i = 0; i < this.children.length; i++) {

            if (this.children[i].selected) {

                selected_array.push(this.children[i].value);
            }

        }

        for (var i = 0; i < selected_array.length; i++) {

            search_string = search_string + '|' + $.fn.dataTable.util.escapeRegex(selected_array[i]);

        }

        $('#standard_results_table').DataTable().column(2).search(search_string ? '^(' + search_string + ')$' : '', true, false)
            .draw();

        STANDARD_FILTER_DETAILS_ARRAY.gender.length = 0

        for (var i = 0; i < selected_array.length; i++) {
            STANDARD_FILTER_DETAILS_ARRAY.gender.push(selected_array[i]);
        }


        $(STANDARD_TRIGGER_OBJECT).trigger("trigger");


    });

    $('#standard_results_table').dataTable().api().on('search.dt', function () {

        if(!STANDARD_TABLE_INITIALISED){

            return;
        }

        $('#standard_results_table').dataTable().api().column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function (cell, i) {



            var row_data = $('#standard_results_table').dataTable().api().row(i).data();

            if (i == 0) {

                cell.innerHTML = i + 1;

            }



            else if (parseInt(row_data[6]) === "NaN") {


                cell.innerHTML = "#";

            }

            else {

                var thisTableApi = $('#standard_results_table').dataTable().api();

                var lastRowData = thisTableApi.row(i - 1).data();

                if (row_data[6] === lastRowData[6]) {

                    var last_unique_position = thisTableApi.row(i - 1).node().children[0].innerHTML;

                    last_unique_position = last_unique_position.replace(/\D/g, '');

                    thisTableApi.row(i - 1).node().children[0].innerHTML = '=' + last_unique_position;

                    cell.innerHTML = '=' + last_unique_position;

                }

                else {
                    cell.innerHTML = i + 1;

                }

            }

        });


    }).draw();

    $('#id_standard_age_filter').SumoSelect({placeholder: 'All'});

    $('#id_standard_gender_filter').SumoSelect({placeholder: 'All'});




});



function secondsToTimeString(seconds){

                var hours = Math.floor(seconds/3600);
                var minutes = Math.floor((seconds - (hours * 3600)) / 60);
                var mSeconds = Math.floor(seconds - (hours * 3600) - (minutes * 60));


                var hours_string = hours<10 ? "0" + hours : hours + "";
                var minutes_string = minutes<10 ? "0" + minutes : minutes + "";
                var seconds_string = mSeconds<10 ? "0" + mSeconds : mSeconds + "";


    if(hours>0){

        return hours_string+":"+minutes_string+":"+seconds_string;
    }

    else{

        return minutes_string+":"+seconds_string;
    }


}

function escapeRegExp(string) {
    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function replaceAll(string, find, replace) {
    return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}