/**
 * Created by drsjmiddleton on 09/08/2016.
 */


var AREA_NAME="New Area";
var NEW_AREA_MARKER;
var LOCAL_USER_AREAS=JSON.parse(USER_AREAS);
var LOCAL_OTHER_AREAS=JSON.parse(OTHER_AREAS);

function loadGmapsScript() {
  var script = document.createElement("script");
  script.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyAXyhsLI4MNfA_lpEPILD15sd7Er8qmMDA&callback=initMap";
  document.body.appendChild(script);
}

function getAreaName(){

    if (AREA_NAME){

        return AREA_NAME;
    }

    else{

        return "New Area";
    }
}

function placeMarker(latLng, map, title, otherUser) {

    if(otherUser){

       new google.maps.Marker({
        icon:'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
           title: title,
        position: latLng,
        map: map
    });
    }
    else{

        new google.maps.Marker({
        title: title,
        position: latLng,
        map: map
    });


    }
   
}

function placeNewMarkerAndPanTo(latLng, map) {

    NEW_AREA_MARKER = new google.maps.Marker({
        title: getAreaName(),
      position: latLng,
    map: map
  });
  map.panTo(latLng);
    


    $('#id_latitude').val(Number(latLng.lat()).toFixed(6));

    $('#id_longitude').val(Number(latLng.lng()).toFixed(6));
}

function initMap() {
        var myLatLng = {lat: 51.508742, lng: -0.120850};


        var area_map = new google.maps.Map(document.getElementById('area_location_map'), {
            zoom: 4,
            center: myLatLng,
            disableDoubleClickZoom: true
        });

    for (var i = 0; i < LOCAL_USER_AREAS.length; i++) {


        placeMarker(new google.maps.LatLng(Number(LOCAL_USER_AREAS[i][2]), Number(LOCAL_USER_AREAS[i][3]), false)
        , area_map, LOCAL_USER_AREAS[i][1], false)

    }

    for (var i = 0; i < LOCAL_OTHER_AREAS.length; i++) {


        placeMarker(new google.maps.LatLng(Number(LOCAL_OTHER_AREAS[i][2]), Number(LOCAL_OTHER_AREAS[i][3]), false)
        , area_map, LOCAL_OTHER_AREAS[i][1], true)

    }

        area_map.addListener('dblclick', function(e) {

            if(NEW_AREA_MARKER){
                NEW_AREA_MARKER.setMap(null);
            }

            placeNewMarkerAndPanTo(e.latLng, area_map)
            });
    }

$(document).ready( function () {


loadGmapsScript();


$('#id_area_name').on('keyup', function(){

    AREA_NAME=$('#id_area_name').val();
});


});

