__author__ = 'drsjmiddleton'


from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, JsonResponse
from django.core.urlresolvers import reverse
from django.core import serializers
from django.contrib import messages
from django.contrib.messages import get_messages
from django.contrib.auth.decorators import login_required
from django.db.models import Q

from iOrienteering.static_values import *

from paypal.standard.forms import PayPalPaymentsForm
from paypal.standard.models import ST_PP_COMPLETED
from paypal.standard.ipn.signals import valid_ipn_received, invalid_ipn_received

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.files.storage import get_storage_class
from pathlib import Path

from website.models import *
from website.website_helper_functions import *
from website.serializers import *

from competitor_portal.competitor_forms import *

from organiser_portal.organiser_json_encoders import *

from django.contrib.auth import authenticate, login, logout
from django.conf import settings


from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes
from rest_framework.authentication import TokenAuthentication

from django.db import IntegrityError

import json

@login_required
def competitor_home(request):

    this_user = request.user

    this_user_profile = get_object_or_404(UserProfile, user=this_user)

    this_user_profile.display_credit="{0:.2f}".format(this_user_profile.credit)

    this_user.number_of_areas = this_user.area_set.all().count()

    messages = list(UserMessage.objects.filter((Q(universal_message=True) | Q(user=this_user))))

    this_user.maps_counter = this_user.map_set.all().count()

    course_competitors = list(this_user_profile.parent_competitor.all())

    for cc in course_competitors:
        cc.competed=cc.hasCompeted()


    warning_messages = []
    info_messages = []

    for message in list(messages):

        if message.expire_date<date.today():

            message.delete()

        elif message.important:
           warning_messages.append(message)
        else:
            info_messages.append(message)

    warning_messages.sort(key=lambda x: x.message_date, reverse=True)
    info_messages.sort(key=lambda x: x.message_date, reverse=True)

    context = {'this_user':this_user, 'info_messages':info_messages,'warning_messages':warning_messages,
               'this_user_profile':this_user_profile, 'course_competitors':course_competitors}

    return render(request, 'competitor/competitor_home.html', context)



@login_required
def competitor_maps(request):

    pass



@login_required
def new_course_competitor(request):

    this_user = request.user

    tempUC = CourseCompetitor(user=this_user)

    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        user_competitor_form = CreateCourseCompetitorForm(request.POST , instance=tempUC)
        # check whether it's valid:
        if user_competitor_form.is_valid():

            try:
                user_competitor_form.save()

                return HttpResponseRedirect(reverse('course_competitors', ))

            except IntegrityError:
                user_competitor_form.non_field_errors = ["This is identical to competitor already in teh area"]

    # if a GET (or any other method) we'll create a blank form
    else:
        user_competitor_form = CreateCourseCompetitorForm(instance=tempUC)


    user_competitor_form.helper.form_action = reverse('new_user_competitor')

    context = {'this_user':this_user , 'user_competitor_form':user_competitor_form }

    return render(request, 'website/new_course_competitor.html', context)


@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@csrf_exempt
def api_competitor_get_area_list(request):

    all_areas = list(Area.objects.all().exclude(public=False))

    serializer = GmapAreaSerializer(all_areas, many=True)

    return JSONResponse(serializer.data)


@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@csrf_exempt
def api_competitor_get_area_course_list(request, area_pk):

    this_area = get_object_or_404(Area, pk=area_pk)

    area_courses = list(Course.objects.all().filter(area=this_area))

    serializer = ViewCourseSerializer(area_courses, many=True)

    return JSONResponse(serializer.data)

@api_view(['POST'])
@authentication_classes((TokenAuthentication,))
@csrf_exempt
def api_competitor_load_result(request):

    this_area = get_object_or_404(Area, pk=int(request.POST.get('area', "")))
    this_course = get_object_or_404(Course, pk=int(request.POST.get('course', "")))

    competitor_user = get_object_or_404(User, username=request.POST.get('username', ""))

    competitor = get_object_or_404(UserProfile, user=competitor_user)

    organiser = get_object_or_404(UserProfile, user=this_area.user)

    comp_status = int(request.POST.get('status', ""))

    points = int(request.POST.get('points', ""))

    run_time = int(request.POST.get('run_time', ""))

    run_date = request.POST.get('run_date', "")

    try:
        rpt_result = Result.objects.get(competitor=competitor,run_date=run_date,course=this_course,run_time=run_time)
        return Response(status=status.HTTP_409_CONFLICT)

    except Result.DoesNotExist:
        pass

    this_result = Result(
                            competitor = competitor,
                            organiser = organiser,
                            run_date=run_date,
                            splits_xml = request.POST.get('splits_xml', ""),
                            status = comp_status,
                            course = this_course,
                            area = this_area,
                            points = points,
                            run_time = run_time,
                            dsq_reason = request.POST.get('dsq_reason', ""),
    )

    try:
        this_result.save()
        return Response(status=status.HTTP_202_ACCEPTED)
    except IntegrityError:
        return Response(status=status.HTTP_406_NOT_ACCEPTABLE)





    # < ?xml
    # version = "1.0"
    # encoding = "ISO-8859-1"? >
    # < !DOCTYPE
    # ResultList
    # SYSTEM
    # "IOFdata.dtd" >
    #
    # < ResultList
    # status = "snapshot" >
    # < IOFVersion
    # version = "2.0.3" / >
    #
    # < EventId
    # type = "int"
    # idManager = "IOF" > WC1 < / EventId >
    #
    # < ClassResult >
    # < ClassShortName > Men < / ClassShortName >
    #
    # < PersonResult >
    # < PersonId > 101 < / PersonId >

# < Result >
# < Time > 27:52.5 < / Time >
# < ResultPosition > 17 < / ResultPosition >
# < CompetitorStatus
# value = "OK" / >
# < SplitTime
# sequence = "1" >
# < ControlCode > 31 < / ControlCode >
# < Time > 2:21 < / Time >
# < / SplitTime >
# < SplitTime
# sequence = "2" >
# < ControlCode > 300 < / ControlCode >
# < Time > 37:52.5 < / Time >
# < / SplitTime >
# < / Result >