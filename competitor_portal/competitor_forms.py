__author__ = 'drsjmiddleton'

from django.forms import ModelForm
from django.core.validators import MinValueValidator, MaxValueValidator, RegexValidator

from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.models import User

from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import *
from crispy_forms.bootstrap import PrependedText

from datetime import datetime
import re

from website.models import *

class CreateCourseCompetitorForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(CreateCourseCompetitorForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_class = 'form-horizontal group-border hover-stripped'
        self.helper.label_class = 'col-lg-2 col-md-2 col-sm-12 control-label'
        self.helper.field_class = 'col-lg-6 col-md-6'
        self.helper.add_input(Submit('submit', 'Submit'))


    class Meta:
        model = CourseCompetitor
        #date_of_birth = DateField(input_formats=['%Y-%m-%d','%d-%m-%Y'])
        exclude=['user',]